import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_stetho/flutter_stetho.dart';
import 'package:notelat_app/src/models/loginModel.dart';
import 'package:notelat_app/src/screens/home.dart';
import 'package:notelat_app/src/screens/login.dart';
import 'package:notelat_app/src/screens/screen_admin/employee/employee.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';

Future<void> main() async {
  Stetho.initialize();
  final token = await StoragePreferences.getToken();
  final role = await StoragePreferences.getRole();
  print(role);
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize();
  
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.blue));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: token == null ? Login() : role == 1 ? EmployeePage() : Home()
    ));
  });
}

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: ,
//     );
//   }
// }
