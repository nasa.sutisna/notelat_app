import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:location_permissions/location_permissions.dart'
    as localPermission;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:notelat_app/src/blocs/attendanceBloc.dart';
import 'package:notelat_app/src/blocs/homeBloc.dart';
import 'package:notelat_app/src/blocs/loginBloc.dart';
import 'package:notelat_app/src/blocs/worklocationBloc.dart';
import 'package:notelat_app/src/screens/login.dart';
import 'package:notelat_app/src/screens/maps.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:notelat_app/src/utills/userData.dart';
import 'package:notelat_app/src/widgets/menu_drawer.dart';
import 'package:toast/toast.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var startTime = '00:00';
  var endTime = '00:00';
  bool _isLoading = false;
  int role;
  var employee;
  var photo;
  bool isMockLocation = false;

  @override
  void initState() {
    attendanceBloc.getTodayAttendance();
    getUserData();
    _checkPermissionLocation();
    requestLocationPermission();
    _checkToken();
    userData.getUserData();
    // TrustLocation.start(5);
    // getLocation();
    super.initState();
  }

  @override
  void dispose() {
    // attendanceBloc.dispose();
    if (mounted) {
      super.dispose();
    }
  }

  void _checkToken() async {
    try {
      var _token = await StoragePreferences.getToken();
      await loginBloc.checkToken(_token);
    } catch (error) {
      var message = error.runtimeType == String ? 'Server Error' : error['message'];
      Toast.show(message, context, duration: Toast.LENGTH_LONG);
      if(message != 'Server Error') {
        await _logout();
      }
    }
  }

  void _checkPermissionLocation() async {
    Location location = Location();

    var permission = await location.hasPermission();

    if (permission != localPermission.PermissionStatus.denied) {
      await location.requestPermission();
    }
  }

  getUserData() async {
    var emp = await homeBloc.getUserData();
    var getRole = await homeBloc.getRole();

    setState(() {
      employee = emp;
      role = getRole;
    });
    worklocationBloc.getWorklocationByEmp(employee['employeeId']);
    await getPhotoProfile();
  }

  getPhotoProfile() async {
    var getPhoto = await homeBloc.showImageProfile(employee["photo"]);
    setState(() {
      photo = getPhoto;
    });
  }

  Future _logout() async {
    try {
      setState(() {
        _isLoading = true;
      });

      var token = await StoragePreferences.getToken();
      await loginBloc.doLogout(token);
      await StoragePreferences.clearStorage();
      setState(() {
        _isLoading = false;
      });

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Login()));
    } catch (error) {
      await StoragePreferences.clearStorage();
      setState(() {
        _isLoading = false;
      });

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Login()));
    }
  }

  /// get location method, use a try/catch PlatformException.
  Future<void> getLocation() async {
    // try {
    //   TrustLocation.onChange.listen((values) => setState(() {
    //         isMockLocation = values.isMockLocation;
    //       }));
    // } on PlatformException catch (e) {
    //   print('PlatformException $e');
    // }
  }

  /// request location permission at runtime.
  void requestLocationPermission() async {
    dynamic permission =
        await localPermission.LocationPermissions().requestPermissions();
    print('permissions: $permission');
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    print('role update');
    role = role;
    super.didUpdateWidget(oldWidget);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text('Kehadiran'),
        ),
        drawer: MenuDrawer(),
        body: ModalProgressHUD(child: preview(), inAsyncCall: _isLoading));
  }

  Widget preview() {
    return SafeArea(
        child: ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            userInfo(),
            showTimeAttendance(),
            showShift(),
            button()
          ],
        )
      ],
    ));
  }

  Widget userInfo() {
    return Container(
        color: Colors.blue,
        height: MediaQuery.of(context).size.height / 2.3,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: employee == null || employee["photo"] == null
                  ? ClipOval(
                      child: Image.asset("assets/default.png",
                          height: 130.0, width: 135.0, fit: BoxFit.fill))
                  : ClipOval(
                      child: FadeInImage.assetNetwork(
                      placeholder: 'assets/default.png',
                      height: 130.0,
                      width: 135.0,
                      image: photo,
                      fit: BoxFit.fill,
                    )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: Text(
                employee != null ? employee['fullName'] : '',
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 2.0),
              child: Text(
                employee != null ? employee['position'] : '',
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
            ),
          ],
        ));
  }

  Widget showTimeAttendance() {
    return StreamBuilder(
      stream: attendanceBloc.responseTodayAttendace,
      builder: (context, AsyncSnapshot snapshot) {
        return SafeArea(
            child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                height: 100,
                width: MediaQuery.of(context).size.width / 1.3,
                child: Padding(
                    padding: EdgeInsets.all(2.0),
                    child: Container(
                      height: 10.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Jam Masuk",
                                style: TextStyle(fontSize: 14.0),
                              ),
                              SizedBox(height: 8),
                              Text(
                                snapshot.hasData &&
                                        snapshot.data['attendance']
                                                ['startTime'] !=
                                            null
                                    ? DateFormat.Hm().format(DateTime.parse(
                                        snapshot.data['attendance']
                                            ['startTime']))
                                    : "00:00",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.green,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0,
                                bottom: 10.0,
                                left: 45.0,
                                right: 45.0),
                            child: VerticalDivider(
                              color: Colors.black87,
                              width: 2.0,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Jam Keluar",
                                style: TextStyle(fontSize: 14.0),
                              ),
                              SizedBox(height: 8),
                              Text(
                                snapshot.hasData &&
                                        snapshot.data['attendance']
                                                ['endTime'] !=
                                            null
                                    ? DateFormat.Hm().format(DateTime.parse(
                                        snapshot.data['attendance']['endTime']))
                                    : "00:00",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.black12,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ),
          ],
        ));
      },
    );
  }

  Widget showShift() {
    return StreamBuilder(
      stream: attendanceBloc.responseTodayAttendace,
      builder: (context, AsyncSnapshot snapshot) {
        return SafeArea(
            child: Column(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Text(
                    snapshot.hasData
                        ? snapshot.data['shiftDaily']['shiftName']
                        : '',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0,
                        color: Colors.black54),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    snapshot.hasData
                        ? snapshot.data['shiftDaily']['shiftTime']
                        : '-',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ],
              ),
            ),
          ],
        ));
      },
    );
  }

  Widget button() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      padding: EdgeInsets.only(top: 35.0),
      child: Column(
        children: <Widget>[
          ButtonTheme(
            height: 45.0,
            minWidth: MediaQuery.of(context).size.width / 1.3,
            child: RaisedButton(
              onPressed: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MapsPage(
                            workLocation: null,
                          )),
                );

                await attendanceBloc.getTodayAttendance();
              },
              child: Text(
                "Rekam Kehadiran",
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
              color: Colors.blue,
              splashColor: Colors.lightBlue,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          ButtonTheme(
              height: 45.0,
              minWidth: MediaQuery.of(context).size.width / 1.3,
              child: OutlineButton(
                  child: Text(
                    "Keluar",
                    style: TextStyle(fontSize: 16.0, color: Colors.blue),
                  ),
                  color: Colors.black,
                  onPressed: () {
                    _logout();
                  },
                  borderSide: BorderSide(color: Colors.blue),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)))),
          SizedBox(
            height: 30.0,
          )
        ],
      ),
    );
  }
}
