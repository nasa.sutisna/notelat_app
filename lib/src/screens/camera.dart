import 'package:flutter/material.dart';
import 'package:notelat_app/src/screens/cameraScreen.dart';

class CameraPage extends StatefulWidget {
  final geolocation;
  CameraPage({Key key, this.geolocation}) : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  final _cameraKey = GlobalKey<CameraScreenState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        backgroundColor: Theme.of(context).backgroundColor,
        body: CameraScreen(
          key: _cameraKey,
          geolocation: widget.geolocation,
        ));
  }
}
