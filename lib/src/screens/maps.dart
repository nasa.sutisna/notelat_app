import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:notelat_app/src/blocs/attendanceBloc.dart';
import 'package:notelat_app/src/screens/camera.dart';
import 'package:notelat_app/src/utills/haversine.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:notelat_app/src/widgets/dialogs.dart';
import 'package:true_time/true_time.dart';
import 'package:trust_location/trust_location.dart';

class MapsPage extends StatefulWidget {
  final workLocation;

  MapsPage({Key key, @required this.workLocation}) : super(key: key);

  @override
  _MapsPageState createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  Completer<GoogleMapController> _controller = Completer();
  dynamic currentLocation;
  List<Marker> allMarkers = [];
  dynamic latitude;
  dynamic longitude;
  dynamic worklocation = {"lat": -6.258821699999999, "lng": 106.72460790000002};
  dynamic radius;
  Set<Circle> circles;
  Marker bbb;
  bool isFirstAttendance = false;
  Dialogs dialog = new Dialogs();
  double distance = 0;

  @override
  void initState() {
    // TrustLocation.start(5);
    _getLocation();
    getWorklocation();
    checkFirstPhoto();
    // if (mounted) {
    //   try {
    //     TrustLocation.onChange.listen((values) => setState(() {
    //           print(' listen $values');
    //         }));
    //   } on PlatformException catch (e) {
    //     print('PlatformException $e');
    //   }
    // }

    super.initState();
  }

  @override
  void dispose() {
    // TrustLocation.stop();
    super.dispose();
  }

  getWorklocation() async {
    final getWorklocation = await StoragePreferences.getWorklocation();
    setState(() {
      radius = double.parse(getWorklocation['radius'].toString());
      worklocation = {
        "lat": double.parse(getWorklocation['latitude']),
        "lng": double.parse(getWorklocation['longitude'])
      };
      setCoordinate(worklocation, radius);
      print(radius);
    });
  }

  checkFirstPhoto() async {
    try {
      await attendanceBloc.checkFirstFoto();
      setState(() {
        isFirstAttendance = false;
      });
    } catch (error) {
      setState(() {
        isFirstAttendance = true;
      });
    }
  }

  setCoordinate(worklocation, radiusOrdinate) {
    print('lat : ${worklocation["lat"]}');
    print('lng : ${worklocation["lng"]}');
    bbb = Marker(
      markerId: MarkerId('bbb'),
      position: LatLng(latitude ?? 0, longitude ?? 0),
      infoWindow: InfoWindow(title: 'Lokasi Kamu'),
      icon: BitmapDescriptor.defaultMarkerWithHue(
        BitmapDescriptor.hueRed,
      ),
    );

    print('radiusOrdinate $radiusOrdinate');
    circles = Set.from([
      Circle(
          circleId: CircleId("1"),
          center: LatLng(worklocation['lat'], worklocation['lng']),
          radius: radiusOrdinate,
          strokeColor: Colors.green[200],
          fillColor: Colors.green[100],
          strokeWidth: 5)
    ]);

    Future.delayed(Duration(seconds: 3)).then((value) {
      var inCoverage =
          Haversine.inCoverage(worklocation, currentLocation, radius);
      print('coverage $inCoverage');

      distance = inCoverage['distance'];
      distance = num.parse(distance.toStringAsFixed(2));
      setState(() {});
    });
    setState(() {});
  }

  void _getLocation() async {
    print('getLocation');

    /// check mock location on Android device.
    // bool isMockLocation = await TrustLocation.isMockLocation;
    // if (isMockLocation) {
    //   print('masuk sini $isMockLocation');
    //   final dialog = new Dialogs();
    //   await dialog.warning(context, "Peringatan",
    //       "Handphone terdeteksi menggunakan lokasi palsu");
    //   Navigator.of(context).pop();
    // }

    Location location = Location();
    var permission = await location.hasPermission();

    if (permission != PermissionStatus.denied) {
      final LocationData pos = await location.getLocation();

      setState(() {
        latitude = pos.latitude ?? 0;
        longitude = pos.longitude ?? 0;
        currentLocation = {"lat": latitude, "lng": longitude};

        allMarkers.add(Marker(
            markerId: MarkerId('myMarker'),
            draggable: false,
            position: LatLng(latitude, longitude),
            onTap: () {
              print('marker tagged');
            }));

        print(currentLocation);
      });
    } else {
      try {
        // await location.requestPermission();
        // _getLocation();
      } catch (error) {
        print(error);
      }
    }
  }

  checkCoverage() async {
    print('check check coverage');
    print('check worklocation $worklocation');
    print('check currentLocation $currentLocation');

    var inCoverage =
        Haversine.inCoverage(worklocation, currentLocation, radius);
    distance = inCoverage['distance'];
    distance = num.parse(distance.toStringAsFixed(2));
    setState(() {});
    print('inCoverage $inCoverage');
    if (inCoverage['status'] == false) {
      presentDialog("Upps, Kamu berada diluar radius tempat kerja");
    } else {
      if (isFirstAttendance) {
        await dialog.warning(context, "Informasi",
            "Kamu akan merekam data kehadiran untuk pertama kali. Mohon pastikan posisi wajah dalam posisi yang jelas ketika pengambilan foto dan pastikan bukan mengambil foto wajah orang lain");
      }

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CameraPage(
                    geolocation:
                        '${currentLocation['lat']},${currentLocation['lng']}',
                  ))).then((value) => Navigator.pop(context));
    }

    return inCoverage;
  }

  presentDialog(String message) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            title: Center(
                child: Text(
              message,
              style: TextStyle(),
            )),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "OK",
                      style: TextStyle(
                          fontWeight: FontWeight.w800, fontSize: 18.0),
                    ),
                  )
                ],
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Lokasi Kehadiran")),
        body: Stack(
          children: <Widget>[
            latitude == null && longitude == null
                ? Container()
                : maps(_controller, allMarkers, latitude, longitude),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 180.0,
                  decoration: BoxDecoration(
                      color: Colors.grey[50],
                      // border: Border.all(color: Colors.grey),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius:
                              10.0, // has the effect of softening the shadow
                          spreadRadius:
                              1.0, // has the effect of extending the shadow
                          offset: Offset(
                            0, // horizontal, move right 10
                            5.0, // vertical, move down 10
                          ),
                        )
                      ],
                      borderRadius: BorderRadius.circular(10)),
                  width: MediaQuery.of(context).size.width,
                  margin:
                      EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                "Apakah lokasi kamu sudah sesuai ?",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 20.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Jarak dari lokasi kerja : $distance meter",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.grey),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                  "Maksimum Radius : $radius meter",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.grey),
                                ),
                              ),
                            ],
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RaisedButton(
                            onPressed: () {
                              _getLocation();
                            },
                            color: Colors.deepOrange,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side:
                                    BorderSide(color: Colors.deepOrange[200])),
                            child: Text(
                              'Refresh',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          RaisedButton(
                            onPressed: () {
                              checkCoverage();
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.green[200])),
                            color: Colors.green,
                            child: Text(
                              'Ya',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          )
                        ],
                      )
                    ],
                  )),
            )
          ],
        ));
  }

  Widget maps(_controller, allMarker, latitude, longitude) {
    return Container(
      child: GoogleMap(
        mapType: MapType.normal,
        cameraTargetBounds: CameraTargetBounds.unbounded,
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        compassEnabled: true,
        initialCameraPosition: CameraPosition(
            target: LatLng(latitude ?? 0, longitude ?? 0), zoom: 17.4746),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        circles: circles,
        markers: Set.from(allMarker),
        zoomGesturesEnabled: true,
      ),
    );
  }
}
