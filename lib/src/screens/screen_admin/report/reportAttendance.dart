
import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:intl/intl.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:notelat_app/src/widgets/menu_drawer.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class ReportAttendancePage extends StatefulWidget {
  @override
  _ReportAttendancePageState createState() => _ReportAttendancePageState();
}

class _ReportAttendancePageState extends State<ReportAttendancePage> {
  final format = DateFormat("dd-MM-yyyy");
  dynamic startDate;
  dynamic endDate;
  String _localPath;

  void requestDownload() async {
    _localPath = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);

    print('localpath $_localPath');
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    print(await _findLocalPath());
    if (hasExisted == false) {
      savedDir.create();
    }
    download(_localPath);
  }

  Future download(_localPath) async {
    await Permission.storage.request();
    final token = await StoragePreferences.getToken();
    final taskId = await FlutterDownloader.enqueue(
      url: configApi.baseUrl + '/report/download?startDate=$startDate&endDate=$endDate&access_token=$token&baseUrl=${configApi.baseUrl}',
      savedDir: _localPath,
      showNotification:
          true, // show download progress in status bar (for Android)
      openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
    );

    print('download done $taskId');
  }

  Future<String> _findLocalPath() async {
    final directory = await getExternalStorageDirectory();
    print(directory);
    return directory.path;
  }

  @override
  void initState() {
    var date = new DateTime.now();
    var totalDay = DateTime(date.year, date.month + 1, 0).day;

    startDate = new DateTime(date.year, date.month, totalDay - (totalDay - 1));
    endDate = new DateTime(date.year, date.month, totalDay);

    print(totalDay);
    print(startDate);
    print(endDate);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Laporan Kehadiran'),
      ),
      body: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(right: 12.0, left: 12.0, top: 12.0),
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                'Tanggal Mulai',
                textAlign: TextAlign.left,
              ),
            ),
            DateTimeField(
              readOnly: true,
              format: format,
              initialValue: startDate,
              onChanged: (value) {
                print('value $value');
                startDate = value;
                setState(() {});
              },
              onShowPicker: (context, currentValue) {
                return showDatePicker(
                    context: context,
                    firstDate: DateTime(1900),
                    initialDate: currentValue ?? DateTime.now(),
                    lastDate: DateTime(2100));
              },
            ),
            Container(
              padding: EdgeInsets.only(top: 16),
              alignment: Alignment.centerLeft,
              child: Text(
                'Tanggal Akhir',
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 16.0),
              child: DateTimeField(
                readOnly: true,
                format: format,
                initialValue: endDate,
                onChanged: (value) {
                  print('value $value');
                  endDate = value;
                  setState(() {});
                },
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                },
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              child: RaisedButton(
                color: Theme.of(context).primaryColor,
                child: Text(
                  'DOWNLOAD',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  requestDownload();
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          ],
        ),
      ),
      drawer: MenuDrawer(),
    );
  }
}
