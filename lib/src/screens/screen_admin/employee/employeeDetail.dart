import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:notelat_app/src/blocs/employeeBloc.dart';
import 'package:notelat_app/src/screens/screen_admin/employee/employeeCreate.dart';
import 'package:toast/toast.dart';

class EmployeeDetailPage extends StatefulWidget {
  final dataEmployee;
  EmployeeDetailPage({Key key, this.dataEmployee}) : super(key: key);

  @override
  _EmployeeDetailPageState createState() => _EmployeeDetailPageState();
}

class _EmployeeDetailPageState extends State<EmployeeDetailPage> {
  dynamic dataEmployee;
  File imageFile;
  File pickImageError;
  String photoUrl;

  @override
  void initState() {
    dataEmployee = widget.dataEmployee;
    getPhotoUrl();
    super.initState();
  }

  @override
  void didUpdateWidget(EmployeeDetailPage oldWidget) {
    print('did upate');
    getPhotoUrl();
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  void moveToCreatePage() async {
    final information = await Navigator.push(
      context,
      CupertinoPageRoute(
          fullscreenDialog: true,
          builder: (context) => EmployeeCreatePage(
                navParams: dataEmployee,
              )),
    );

    dataEmployee = information ?? dataEmployee;
    setState(() {});
  }

  void takePicture() async {
    try {
      imageFile = await ImagePicker.pickImage(
          source: ImageSource.gallery,
          maxWidth: 500,
          maxHeight: 500,
          imageQuality: 70);

      setState(() {});
      if (imageFile != null) {
        updatePhoto();
      }
    } catch (e) {
      pickImageError = e;
    }
  }

  getPhotoUrl() async {
    photoUrl = dataEmployee['photo'] != null && dataEmployee['photo'] != ''
        ? await employeeBloc.getImageProfile(dataEmployee['photo'])
        : null;
    setState(() {});
    print(' photo url $photoUrl');
  }

  updatePhoto() async {
    try {
      final result = await employeeBloc.editEmployee(imageFile, dataEmployee);
      print(result);
      Toast.show("Ubah foto berhasil", context);
    } catch (error) {
      print('error $error');
      Toast.show("Gagal ubah foto", context);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Detail Karyawan')),
        body: ListView(
          children: <Widget>[
            Container(
                child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  color: Colors.blue,
                  child: Center(
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: ClipOval(
                            child: imageFile != null
                                ? Image.file(
                                    imageFile,
                                    height: 120,
                                    width: 120,
                                    fit: BoxFit.fill,
                                  )
                                : photoUrl != null
                                    ? FadeInImage.assetNetwork(
                                        placeholder: 'assets/default.png',
                                        height: 120.0,
                                        width: 120.0,
                                        image: photoUrl,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.asset(
                                        'assets/default.png',
                                        height: 120,
                                      ),
                          ),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: EdgeInsets.only(left: 80.0, bottom: 30),
                              child: Container(
                                height: 40.0,
                                width: 40.0,
                                child: FittedBox(
                                  child: FloatingActionButton(
                                      backgroundColor: Colors.grey[500],
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        takePicture();
                                      }),
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Stack(
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, left: 26),
                                child: Text(
                                  'Informasi Pribadi',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 180, top: 0),
                                child: IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      moveToCreatePage();
                                    }),
                              ),
                            ],
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width / 1,
                  child: Card(
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, top: 10.0),
                            child: Text('Nomor Karyawan'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, top: 5),
                            child: Text(
                              dataEmployee['employeeNumber'],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, top: 10.0),
                            child: Text('Nama Lengkap'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, top: 5),
                            child: Text(
                              dataEmployee['fullName'],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, top: 16.0),
                            child: Text('Posisi'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, top: 5),
                            child: Text(
                              dataEmployee['position'],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, top: 16.0),
                            child: Text('Email'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, top: 5),
                            child: Text(
                              dataEmployee['email'],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, top: 16.0),
                            child: Text('Nomor Handphone'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, top: 5, bottom: 16),
                            child: Text(
                              dataEmployee['phoneNumber'],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )),
          ],
        ));
  }
}
