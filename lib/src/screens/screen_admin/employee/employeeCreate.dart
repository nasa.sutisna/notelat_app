import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:notelat_app/src/blocs/employeeBloc.dart';
import 'package:notelat_app/src/widgets/dialogs.dart';
import 'package:toast/toast.dart';

class EmployeeCreatePage extends StatefulWidget {
  final navParams;

  EmployeeCreatePage({Key key, this.navParams}) : super(key: key);
  @override
  _EmployeeCreatePageState createState() => _EmployeeCreatePageState();
}

class _EmployeeCreatePageState extends State<EmployeeCreatePage> {
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _isUpdate = false;
  File _imageFile;
  String _imageUrl;
  dynamic pickImageError;
  bool isVideo = false;
  String empId;
  bool _isLoading = false;
  String shiftGroupCode = '';
  String shiftGroupName = '';

  TextEditingController _employeeNumber,
      _fullName,
      _phoneNumber,
      _email,
      _position;

  @override
  void initState() {
    // shiftGroupCode = 'asdfa';
    if (widget.navParams != null) {
      _isUpdate = true;
      empId = widget.navParams['employeeId'];

      _fullName = TextEditingController(text: widget.navParams['fullName']);
      _employeeNumber =
          TextEditingController(text: widget.navParams['employeeNumber']);
      _email = TextEditingController(text: widget.navParams['email']);
      _phoneNumber =
          TextEditingController(text: widget.navParams['phoneNumber']);
      _position = TextEditingController(text: widget.navParams['position']);
      employeeBloc.getImageProfile(widget.navParams['photo']).then((value) {
        _imageUrl = value;
        print(_imageUrl);
      });
    } else {
      _fullName = TextEditingController();
      _employeeNumber = TextEditingController();
      _email = TextEditingController();
      _phoneNumber = TextEditingController();
      _position = TextEditingController();
    }
    super.initState();
  }

  void takePicture() async {
    try {
      _imageFile = await ImagePicker.pickImage(
          source: ImageSource.gallery,
          maxWidth: 500,
          maxHeight: 500,
          imageQuality: 70);

      setState(() {});
    } catch (e) {
      pickImageError = e;
    }
  }

  void saveEmployee() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      setState(() {
        _isLoading = true;
      });

      final body = {
        "employeeNumber": _employeeNumber.text,
        "fullName": _fullName.text,
        "phoneNumber": _phoneNumber.text,
        "email": _email.text,
        "position": _position.text,
      };

      try {
        var result;
        if (_isUpdate) {
          body["employeeId"] = widget.navParams['employeeId'];
          result = await employeeBloc.editEmployee(_imageFile, body);
        } else {
          if (_imageFile == null) {
            setState(() {
              _isLoading = false;
            });
            Toast.show("Foto tidak boleh kosong", context);
            return;
          }

          result = await employeeBloc.addEmployee(_imageFile, body);
        }
        Navigator.pop(context, body);

        Toast.show('Data berhasil disimpan', context);
      } catch (error) {
        print('error $error');
        Toast.show('Data gagal disimpan', context);
      }

      setState(() {
        _isLoading = false;
      });
    }
  }

  void deleteEmployee() async {
    final dialog = new Dialogs();
    final confirm = await dialog.confirm(
        context, "Konfirmasi", "Yakin akan menghapus data karyawan ini?");
    if (confirm == true) {
      try {
        setState(() {
          _isLoading = true;
        });
        final body = {
          "employeeId": widget.navParams['employeeId'],
          "deleted": 1
        };

        final result = await employeeBloc.editEmployee(_imageFile, body);
        setState(() {
          _isLoading = false;
        });

        Toast.show("Data berhasil dihapus", context,
            duration: Toast.LENGTH_LONG);
        Navigator.popUntil(
          context,
          ModalRoute.withName('employeeList'),
        );
      } catch (error) {
        setState(() {
          _isLoading = false;
        });
        Toast.show("Gagal dihapus", context);
      }
    }
  }

  String validator(String value) {
    if (value.isEmpty)
      return "Tidak boleh kosong";
    else
      return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: _isUpdate == true
              ? Text('Edit Karyawan')
              : Text('Tambah Karyawan'),
          actions: _isUpdate == true
              ? <Widget>[
                  IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        deleteEmployee();
                      })
                ]
              : null,
        ),
        body: ModalProgressHUD(child: formAction(), inAsyncCall: _isLoading));
  }

  Widget formAction() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          autovalidate: _validate,
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _employeeNumber,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nomor Karyawan'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _fullName,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Nama lengkap'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _email,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Email'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _phoneNumber,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nomor handphone'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _position,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Jabatan kerja'),
                  validator: validator,
                ),
              ),
              // DropdownButton<String>(
              //   value: shiftGroupName,
              //   items: <Map>[
              //     {"id": "asdfa", "Name": "Nasa Sutisna"},
              //     {"id": "asdgsdasa", "Name": "Andi"}
              //   ].map((Map value) {
              //     return DropdownMenuItem<String>(
              //       value: value['id'],
              //       child: Text(value['Name']),
              //     );
              //   }).toList(),
              //   onChanged: (value) {
              //     print('selected $value');
              //     setState(() {
              //       shiftGroupName = value;
              //     });
              //   },
              // ),
              _isUpdate == false ? imageForm() : Container(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'SIMPAN',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    saveEmployee();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget imageForm() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25.0),
      child: Material(
          child: InkWell(
        onTap: () {
          takePicture();
        },
        child: Container(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: _imageFile != null
                ? Stack(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.file(
                              _imageFile,
                              width: 120.0,
                              height: 120.0,
                              fit: BoxFit.fill,
                            ),
                          ),
                          IconButton(
                              icon: Icon(
                                Icons.close,
                                color: Colors.red,
                                size: 35,
                              ),
                              onPressed: () {
                                setState(() {
                                  _imageFile = null;
                                });
                              })
                        ],
                      ),
                    ],
                  )
                : Image.asset('assets/icon-add-image.png',
                    width: 80.0, height: 80.0),
          ),
        ),
      )),
    );
  }
}
