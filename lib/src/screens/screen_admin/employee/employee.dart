import 'dart:async';

import 'package:flutter/material.dart';
import 'package:notelat_app/src/blocs/employeeBloc.dart';
import 'package:notelat_app/src/screens/screen_admin/employee/employeeCreate.dart';
import 'package:notelat_app/src/screens/screen_admin/employee/employeeDetail.dart';
import 'package:notelat_app/src/widgets/menu_drawer.dart';

class EmployeePage extends StatefulWidget {
  @override
  _EmployeePageState createState() => _EmployeePageState();
}

class _EmployeePageState extends State<EmployeePage> {
  List dataEmployees = [];
  TextEditingController keyword;
  bool isSearching = false;
  Timer _debounce;

  @override
  void initState() {
    keyword = TextEditingController();
    employeeBloc.getEmployees(1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: !isSearching
            ? Text('Karyawan')
            : TextField(
                autofocus: true,
                onChanged: (value) {
                  if (_debounce?.isActive ?? false) _debounce.cancel();
                  _debounce = Timer(const Duration(milliseconds: 500), () {
                    // do something with _searchQuery.text
                    employeeBloc.getEmployees(1, keyword: value);
                  });
                },
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    hintText: "Cari...",
                    hintStyle: TextStyle(color: Colors.white)),
              ),
        actions: <Widget>[
          IconButton(
              icon: !isSearching ? Icon(Icons.search) : Icon(Icons.close),
              onPressed: () {
                setState(() {
                  isSearching = !isSearching;
                  if (!isSearching) {
                    employeeBloc.getEmployees(1, keyword: '');
                  }
                });
              })
        ],
      ),
      body: StreamBuilder(
          stream: employeeBloc.dataEmployees,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return EmployeeList(
                dataEmployee: snapshot.data['data'],
                totalPage: snapshot.data['totalPage'],
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
          elevation: 0.0,
          child: Icon(Icons.add),
          backgroundColor: Colors.lightBlue,
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(
                    builder: (context) => EmployeeCreatePage()))
                .then((value) {
              employeeBloc.getEmployees(1);
            });
          }),
      drawer: MenuDrawer(),
    );
  }
}

class EmployeeList extends StatefulWidget {
  final dataEmployee;
  final totalPage;
  EmployeeList({Key key, this.dataEmployee, this.totalPage}) : super(key: key);

  @override
  _EmployeeListState createState() => _EmployeeListState();
}

class _EmployeeListState extends State<EmployeeList> {
  ScrollController scrollController = new ScrollController();
  List employees;
  int currentPage = 1;

  getMoreData() {
    employeeBloc.getEmployees(currentPage + 1, infinite: true).then((value) {
      final response = value;
      List data = response['data'];
      currentPage = response['page'];

      data.forEach((element) {
        setState(() {
          employees.add(element);
        });
      });
    });
  }

  @override
  void initState() {
    employees = widget.dataEmployee;
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (currentPage < widget.totalPage) {
          getMoreData();
        }
      }
    });
    super.initState();
  }

  @override
  void didUpdateWidget(EmployeeList oldWidget) {
    employees = widget.dataEmployee;
    currentPage = 1;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    super.dispose();
  }
  Future<void> _getData() async {
    setState(() {
      employeeBloc.getEmployees(1);
    });
  }
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(child:ListView.builder(
        itemCount: employees.length,
        controller: scrollController,
        itemBuilder: (BuildContext context, int i) {
          return Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Card(
                child: InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (contex) => EmployeeDetailPage(
                                  dataEmployee: employees[i],
                                )))
                        .then((value) => employeeBloc.getEmployees(1));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(
                            '${employees[i]["fullName"]}',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Text(
                          employees[i]['position'],
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                ),
              ));
        }), onRefresh:_getData);
  }
}
