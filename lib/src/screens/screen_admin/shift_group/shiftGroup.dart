import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notelat_app/src/blocs/shiftBloc.dart';
import 'package:notelat_app/src/screens/screen_admin/shift_group/shiftGroupCreate.dart';
import 'package:notelat_app/src/widgets/menu_drawer.dart';

class ShiftGroupPage extends StatefulWidget {
  @override
  _ShiftGroupPageState createState() => _ShiftGroupPageState();
}

class _ShiftGroupPageState extends State<ShiftGroupPage> {
  List listData = [];
  TextEditingController keyword;
  bool isSearching = false;
  Timer _debounce;

  @override
  void initState() {
    keyword = TextEditingController();
    shiftBloc.getListShiftGroup(1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: !isSearching
            ? Text('Shift Group')
            : TextField(
                autofocus: true,
                onChanged: (value) {
                  if (_debounce?.isActive ?? false) _debounce.cancel();
                  _debounce = Timer(const Duration(milliseconds: 500), () {
                    // do something with _searchQuery.text
                    shiftBloc.getListShiftGroup(1, keyword: value);
                  });
                },
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    hintText: "Cari...",
                    hintStyle: TextStyle(color: Colors.white)),
              ),
        actions: <Widget>[
          IconButton(
              icon: !isSearching ? Icon(Icons.search) : Icon(Icons.close),
              onPressed: () {
                setState(() {
                  isSearching = !isSearching;
                  if (!isSearching) {
                    shiftBloc.getListShiftGroup(1, keyword: '');
                  }
                });
              })
        ],
      ),
      body: StreamBuilder(
          stream: shiftBloc.listDataShifGroup,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ShiftList(
                listData: snapshot.data['data'],
                totalPage: snapshot.data['totalPage'],
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
          elevation: 0.0,
          child: Icon(Icons.add),
          backgroundColor: Colors.lightBlue,
          onPressed: () {
            Navigator.of(context)
                .push(
                    MaterialPageRoute(builder: (context) => ShiftGroupCreatePage()))
                .then((value) {
              shiftBloc.getListShiftGroup(1);
            });
          }),
      drawer: MenuDrawer(),
    );
  }
}

class ShiftList extends StatefulWidget {
  final listData;
  final totalPage;
  ShiftList({Key key, this.listData, this.totalPage}) : super(key: key);

  @override
  _ShiftListState createState() => _ShiftListState();
}

class _ShiftListState extends State<ShiftList> {
  ScrollController scrollController = new ScrollController();
  List listData;
  int currentPage = 1;

  getMoreData() {
    shiftBloc.getListShiftGroup(currentPage + 1, infinite: true).then((value) {
      final response = value;
      List data = response['data'];
      currentPage = response['page'];

      data.forEach((element) {
        setState(() {
          listData.add(element);
        });
      });
    });
  }

  @override
  void initState() {
    listData = widget.listData;
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (currentPage < widget.totalPage) {
          getMoreData();
        }
      }
    });
    super.initState();
  }

  @override
  void didUpdateWidget(ShiftList oldWidget) {
    listData = widget.listData;
    currentPage = 1;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: ListView.builder(
          itemCount: listData.length,
          controller: scrollController,
          itemBuilder: (BuildContext context, int i) {
            return Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                child: Card(
                  elevation: 10,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .push(CupertinoPageRoute(
                            fullscreenDialog: true,
                              builder: (context) => ShiftGroupCreatePage(
                                    navParams: listData[i],
                                  )))
                          .then((value) => shiftBloc.getListShiftGroup(1));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            listData[i]['shiftGroupName'],
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.bold),
                          ),
                          ],
                      ),
                    ),
                  ),
                ));
          }),
    );
  }
}
