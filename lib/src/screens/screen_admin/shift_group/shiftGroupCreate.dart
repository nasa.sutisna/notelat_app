import 'package:flutter/material.dart';
import 'package:flutter_multiselect/flutter_multiselect.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:notelat_app/src/blocs/employeeBloc.dart';
import 'package:notelat_app/src/blocs/shiftBloc.dart';
import 'package:notelat_app/src/models/shiftModel.dart';
import 'package:notelat_app/src/widgets/dialogs.dart';
import 'package:toast/toast.dart';

class ShiftGroupCreatePage extends StatefulWidget {
  final navParams;

  ShiftGroupCreatePage({Key key, this.navParams}) : super(key: key);
  @override
  _ShiftGroupCreatePageState createState() => _ShiftGroupCreatePageState();
}

class _ShiftGroupCreatePageState extends State<ShiftGroupCreatePage> {
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _isUpdate = false;
  bool _isLoading = false;
  bool _isLoadingGetEmp = false;
  String shiftCode = '';
  String shiftGroupCode = '';
  String shiftGroupName = '';
  String startTime;
  String endTime;
  TimeOfDay timeOfDay = TimeOfDay.now();
  String startDateTime;
  String endDateTime;
  List<DataShift> shift;
  String selectedShiftMonday;
  String selectedShiftTuesday;
  String selectedShiftWednesday;
  String selectedShiftThursday;
  String selectedShiftFriday;
  String selectedShiftSaturday;
  String selectedShiftSunday;
  TextEditingController _shiftGroupName;
  List employeeSelected = [];
  List employeeList = [];

  @override
  void initState() {
    initializeDateFormatting('in_ID');
    if (widget.navParams != null) {
      _isUpdate = true;
      shiftGroupCode = widget.navParams['shiftGroupCode'];
      _shiftGroupName =
          TextEditingController(text: widget.navParams['shiftGroupName']);
    } else {
      _shiftGroupName = TextEditingController();
    }

    getListShift();
    super.initState();
  }

  List<DropdownMenuItem> generateItems(List<DataShift> shift) {
    List<DropdownMenuItem> items = [];
    for (var item in shift) {
      items.add(DropdownMenuItem(
        child: Text(
            '${item.shiftName} : ${DateFormat.Hm().format(DateTime.parse(item.shiftStartTime.toIso8601String()))} - ${DateFormat.Hm().format(DateTime.parse(item.shiftEndTime.toIso8601String()))}'),
        value: item.shiftCode,
      ));
    }
    return items;
  }

  Future getListShift() async {
    try {
      setState(() {
        _isLoadingGetEmp = true;
      });

      final ShiftModel shift = await shiftBloc.getListShift(1, limit: 100);
      generateItems(shift.data);
      getEmployeeList();
    } catch (error) {
      setState(() {
        _isLoadingGetEmp = false;
      });
      print('error getListShift $error');
    }
  }

  Future getDailyShift(shiftGroupCode) async {
    try {
      final result = await shiftBloc.getShiftDaily(shiftGroupCode);
      formatDayNumber(result['data']['dailyShift']);
      employeeSelected = result['data']['shiftGroupEmp'];
      _isLoadingGetEmp = false;
      setState(() {});
    } catch (error) {
      _isLoadingGetEmp = false;
      setState(() {});
      print('error get getDailyShift $error');
    }
  }

  void formatDayNumber(List data) {
    if (data.length > 0) {
      data.forEach((element) {
        final shiftDayNumber = element['shiftDayNumber'].toString();
        final shiftCode = element['shiftCode'];
        if (shiftDayNumber == '1') {
          selectedShiftMonday = shiftCode;
        } else if (shiftDayNumber == '2') {
          selectedShiftTuesday = shiftCode;
        } else if (shiftDayNumber == '3') {
          selectedShiftWednesday = shiftCode;
        } else if (shiftDayNumber == '4') {
          selectedShiftThursday = shiftCode;
        } else if (shiftDayNumber == '5') {
          selectedShiftFriday = shiftCode;
        } else if (shiftDayNumber == '6') {
          selectedShiftSaturday = shiftCode;
        } else if (shiftDayNumber == '0') {
          selectedShiftSunday = shiftCode;
        }

        setState(() {});
      });
    }
  }

  void saveForm() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      setState(() {
        _isLoading = true;
      });

      List daily = [
        selectedShiftSunday,
        selectedShiftMonday,
        selectedShiftTuesday,
        selectedShiftWednesday,
        selectedShiftThursday,
        selectedShiftFriday,
        selectedShiftSaturday
      ];

      if (daily.indexWhere((element) => element == null) != -1) {
        Toast.show("Shift harus dilengkapi", context,
            duration: Toast.LENGTH_LONG);
        toggleIsLoading(false);
        return;
      }

      if (employeeSelected.length == 0) {
        Toast.show("Karyawan tidak boleh kosong", context,
            duration: Toast.LENGTH_LONG);
        toggleIsLoading(false);
        return;
      }

      final body = {
        "shiftGroupName": _shiftGroupName.text,
        "shiftDaily": daily,
        "employeeSelected": employeeSelected
      };

      try {
        var result;
        if (_isUpdate) {
          body["shiftGroupCode"] = widget.navParams['shiftGroupCode'];
          result = await shiftBloc.editShiftGroup(body);
        } else {
          result = await shiftBloc.addShiftGroup(body);
        }

        Navigator.pop(context, body);
        Toast.show('Data berhasil disimpan', context,
            duration: Toast.LENGTH_LONG);
      } catch (error) {
        Toast.show(
          error.toString(),
          context,
          duration: Toast.LENGTH_LONG,
        );
      }

      setState(() {
        _isLoading = false;
      });
    }
  }

  void toggleIsLoading(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  void getEmployeeList() async {
    try {
      setState(() {
        _isLoadingGetEmp = true;
      });
      
      final result = await employeeBloc.getEmployees(1, limit: 1000, module: widget.navParams == null ? 'shift' : '');

      if (widget.navParams != null) {
        getDailyShift(shiftGroupCode);
      } else {
        _isLoadingGetEmp = false;
      }

      setState(() {
        employeeList = result['data'];
      });
    } catch (error) {
      _isLoadingGetEmp = false;
      setState(() {});
    }
  }

  void deleteShift() async {
    final dialog = new Dialogs();
    final confirm = await dialog.confirm(
        context, "Konfirmasi", "Yakin akan menghapus data ini?");
    if (confirm == true) {
      try {
        setState(() {
          _isLoading = true;
        });

        final body = {"shiftGroupCode": shiftGroupCode, "deleted": 1};

        final result = await shiftBloc.addShift(body);
        setState(() {
          _isLoading = false;
        });

        Toast.show("Data berhasil dihapus", context,
            duration: Toast.LENGTH_LONG);
        Navigator.popUntil(
          context,
          ModalRoute.withName('employeeList'),
        );
      } catch (error) {
        setState(() {
          _isLoading = false;
        });
        Toast.show("Gagal dihapus", context);
      }
    }
  }

  String validator(String value) {
    if (value.isEmpty)
      return "Tidak boleh kosong";
    else
      return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: _isUpdate == true
                ? Text('Edit Shift')
                : Text('Tambah Shift Group'),
            actions: null),
        body: ModalProgressHUD(
            child: formAction(), inAsyncCall: _isLoading || _isLoadingGetEmp));
  }

  Widget formAction() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          autovalidate: _validate,
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _shiftGroupName,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nama Shift Group'),
                  validator: validator,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(top: 8, bottom: 16),
                  child: Text(
                    "Pilih Shift",
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              dropdownShift(),
              multipleSelect(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'SIMPAN',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    saveForm();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dropdownShift() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(bottom: 20.0),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Senin : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftMonday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftMonday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Selasa : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftTuesday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftTuesday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Rabu : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftWednesday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftWednesday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Kamis : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftThursday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftThursday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Jumat : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftFriday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftFriday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Sabtu : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftSaturday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftSaturday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  'Minggu : ',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              StreamBuilder(
                  stream: shiftBloc.listData,
                  builder: (context, AsyncSnapshot<ShiftModel> snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButton(
                        isExpanded: true,
                        value: selectedShiftSunday,
                        items: generateItems(snapshot.data.data),
                        onChanged: (item) {
                          print(item);
                          selectedShiftSunday = item;
                          setState(() {});
                        },
                      );
                    }
                    return Container(
                      child: Text('loading...'),
                    );
                  }),
            ],
          ),
        ],
      ),
    );
  }

  Widget multipleSelect() {
    return _isLoadingGetEmp == true
        ? Container()
        : Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: MultiSelect(
                autovalidate: true,
                initialValue: employeeSelected,
                titleText: 'Karyawan',
                validator: (dynamic value) {
                  if (value == null) {
                    return 'Silahkan pilih satu atau lebih';
                  }
                  return null;
                },
                errorText: 'Silahkan pilih satu atau lebih',
                dataSource: employeeList,
                textField: 'fullName',
                valueField: 'employeeId',
                filterable: true,
                onSaved: (value) {
                  setState(() {
                    print('The value is $value');
                    employeeSelected = value;
                  });
                }),
          );
  }
}
