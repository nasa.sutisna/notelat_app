import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:notelat_app/src/blocs/shiftBloc.dart';
import 'package:notelat_app/src/models/shiftModel.dart';
import 'package:notelat_app/src/widgets/dialogs.dart';
import 'package:toast/toast.dart';

class ShiftCreatePage extends StatefulWidget {
  final DataShift navParams;

  ShiftCreatePage({Key key, this.navParams}) : super(key: key);
  @override
  _ShiftCreatePageState createState() => _ShiftCreatePageState();
}

class _ShiftCreatePageState extends State<ShiftCreatePage> {
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _isUpdate = false;
  bool _isLoading = false;
  String shiftCode;
  String shiftGroupCode = '';
  String shiftGroupName = '';
  String startTime;
  String endTime;
  TimeOfDay timeOfDay = TimeOfDay.now();
  String startDateTime;
  String endDateTime;

  TextEditingController _shiftName;
  @override
  void initState() {
    initializeDateFormatting('in_ID');
    if (widget.navParams != null) {
      _isUpdate = true;
      shiftCode = widget.navParams.shiftCode;
      startDateTime = widget.navParams.shiftStartTime.toIso8601String();
      endDateTime = widget.navParams.shiftEndTime.toIso8601String();

      startTime = DateFormat.Hm().format(DateTime.parse(startDateTime));
      endTime = DateFormat.Hm().format(DateTime.parse(endDateTime));
      _shiftName = TextEditingController(text: widget.navParams.shiftName);
    } else {
      _shiftName = TextEditingController();
    }
    super.initState();
  }

  Future selectTime(BuildContext context, String type) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: timeOfDay,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
            child: child,
          );
        });

    formatTimeOfDay(picked, type);
  }

  void formatTimeOfDay(TimeOfDay tod, String type) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    final format = DateFormat.Hm();

    if (type == 'startTime') {
      startDateTime = dt.toIso8601String();
      startTime = format.format(dt);
      setState(() {});
    } else {
      endDateTime = dt.toIso8601String();
      endTime = format.format(dt);
      setState(() {});
    }
  }

  void saveShift() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      setState(() {
        _isLoading = true;
      });

      final body = {
        "shiftName": _shiftName.text,
        "shiftStartTime": startDateTime != null ? startDateTime : '',
        "shiftEndTime": endDateTime != null ? endDateTime : ''
      };

      try {
        var result;
        if (_isUpdate) {
          body["shiftCode"] = widget.navParams.shiftCode;
          result = await shiftBloc.editShift(body);
        } else {
          if (startDateTime == null || endDateTime == null) {
            setState(() {
              _isLoading = false;
            });

            Toast.show("Jam masuk atau keluar tidak boleh kosong", context,
                duration: Toast.LENGTH_LONG);
            return;
          }

          result = await shiftBloc.addShift(body);
        }

        Navigator.pop(context, body);
        Toast.show('Data berhasil disimpan', context);
      } catch (error) {
        print('error $error');
        Toast.show('Data gagal disimpan', context);
      }

      setState(() {
        _isLoading = false;
      });
    }
  }

  void deleteShift() async {
    final dialog = new Dialogs();
    final confirm = await dialog.confirm(
        context, "Konfirmasi", "Yakin akan menghapus data ini?");
    if (confirm == true) {
      try {
        setState(() {
          _isLoading = true;
        });

        final body = {"shiftCode": shiftCode, "deleted": 1};

        final result = await shiftBloc.addShift(body);
        setState(() {
          _isLoading = false;
        });

        Toast.show("Data berhasil dihapus", context,
            duration: Toast.LENGTH_LONG);
        Navigator.popUntil(
          context,
          ModalRoute.withName('employeeList'),
        );
      } catch (error) {
        setState(() {
          _isLoading = false;
        });
        Toast.show("Gagal dihapus", context);
      }
    }
  }

  String validator(String value) {
    if (value.isEmpty)
      return "Tidak boleh kosong";
    else
      return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title:
                _isUpdate == true ? Text('Edit Shift') : Text('Tambah Shift'),
            actions: null),
        body: ModalProgressHUD(child: formAction(), inAsyncCall: _isLoading));
  }

  Widget formAction() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          autovalidate: _validate,
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _shiftName,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Nama Shift'),
                  validator: validator,
                ),
              ),
              Container(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            'Jam Masuk : ',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                          startTime != null
                              ? Text(startTime,
                                  style: TextStyle(fontSize: 16.0))
                              : Text('--:--'),
                          IconButton(
                              icon: Icon(Icons.timer),
                              onPressed: () {
                                selectTime(context, 'startTime');
                              }),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            'Jam Keluar : ',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                          endTime != null
                              ? Text(endTime, style: TextStyle(fontSize: 16.0))
                              : Text('--:--'),
                          IconButton(
                              icon: Icon(Icons.timer),
                              onPressed: () {
                                selectTime(context, 'endTime');
                              })
                        ],
                      )
                    ],
                  )),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'SIMPAN',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    saveShift();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
