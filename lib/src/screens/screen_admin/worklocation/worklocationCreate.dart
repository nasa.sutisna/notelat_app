import 'package:flutter/material.dart';
import 'package:flutter_multiselect/flutter_multiselect.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:notelat_app/src/blocs/employeeBloc.dart';
import 'package:notelat_app/src/blocs/worklocationBloc.dart';
import 'package:notelat_app/src/models/worklocationModel.dart';
import 'package:notelat_app/src/screens/mapSearch.dart';
import 'package:toast/toast.dart';

class WorklocationCreatePage extends StatefulWidget {
  final DataWorklocation navParams;

  WorklocationCreatePage({Key key, this.navParams}) : super(key: key);
  @override
  _WorklocationCreatePageState createState() => _WorklocationCreatePageState();
}

class _WorklocationCreatePageState extends State<WorklocationCreatePage> {
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _isUpdate = false;
  bool _isLoading = false;
  bool _isLoadingGetEmp = false;
  int worklocationId;
  String worklocationName = '';
  List employeeList = [];
  List employeeSelected = [];
  bool isSwitched = false;
  TextEditingController _worklocationName, _radius, _latitude, _longitude;

  @override
  void initState() {
    initializeDateFormatting('in_ID');
    print(widget.navParams);
    if (widget.navParams != null) {
      _isUpdate = true;
      worklocationId = widget.navParams.worklocationId;
      _worklocationName =
          TextEditingController(text: widget.navParams.worklocationName);
      _radius = TextEditingController(text: widget.navParams.radius.toString());
      _latitude = TextEditingController(text: widget.navParams.latitude);
      _longitude = TextEditingController(text: widget.navParams.longitude);
    } else {
      _worklocationName = TextEditingController();
      _radius = TextEditingController();
      _latitude = TextEditingController();
      _longitude = TextEditingController();
    }

    getEmployeeList();
    super.initState();
  }

  void getEmployeeList() async {
    try {
      setState(() {
        _isLoadingGetEmp = true;
      });

      final result = await employeeBloc.getEmployees(1,
          limit: 1000, module: widget.navParams == null ? 'worklocation' : '');

      _isLoadingGetEmp = false;
      setState(() {
        employeeList = result['data'];
      });

      if (widget.navParams != null) {
        getDetail();
      }
    } catch (error) {
      _isLoadingGetEmp = false;
      setState(() {});
    }
  }

  void getDetail() async {
    try {
      setState(() {
        _isLoadingGetEmp = true;
      });

      final result = await worklocationBloc.detail(worklocationId);

      _isLoadingGetEmp = false;
      setState(() {
        employeeSelected = result['data']['employeeIds'];
        print('employeeSelected $employeeSelected');
      });
    } catch (error) {
      _isLoadingGetEmp = false;
      setState(() {});
    }
  }

  void saveForm() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      setState(() {
        _isLoading = true;
      });

      final body = {
        "worklocationName": _worklocationName.text,
        "latitude": _latitude.text,
        "longitude": _longitude.text,
        "radius": _radius.text,
        "employeeSelected": employeeSelected
      };

      try {
        var result;
        if (_isUpdate) {
          body["worklocationId"] = widget.navParams.worklocationId;
          result = await worklocationBloc.edit(body);
        } else {
          result = await worklocationBloc.add(body);
        }

        Navigator.pop(context, body);
        Toast.show('Data berhasil disimpan', context,
            duration: Toast.LENGTH_LONG);
      } catch (error) {
        Toast.show(
          error.toString(),
          context,
          duration: Toast.LENGTH_LONG,
        );
      }

      setState(() {
        _isLoading = false;
      });
    }
  }

  String validator(String value) {
    if (value.isEmpty)
      return "Tidak boleh kosong";
    else
      return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: _isUpdate == true
                ? Text('Edit Lokasi Kerja')
                : Text('Tambah Lokasi Kerja'),
            actions: null),
        body: ModalProgressHUD(
            child: formAction(), inAsyncCall: _isLoading || _isLoadingGetEmp));
  }

  Widget formAction() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          autovalidate: _validate,
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _worklocationName,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nama Lokasi Kerja'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _longitude,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => MapSearch(
                                  latitude: _latitude.text,
                                  longitude: _longitude.text,
                                )))
                        .then((value) {
                      if (value != null) {
                        print('value 1 $value');
                        _latitude =
                            TextEditingController(text: value['latitude']);
                        _longitude =
                            TextEditingController(text: value['longitude']);
                        setState(() {});
                      }
                    });
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Latitude'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _latitude,
                  keyboardType: TextInputType.number,
                  readOnly: true,
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => MapSearch(
                                  latitude: _latitude.text,
                                  longitude: _longitude.text,
                                )))
                        .then((value) {
                      if (value != null) {
                        print('value $value');
                        _latitude =
                            TextEditingController(text: value['latitude']);
                        _longitude =
                            TextEditingController(text: value['longitude']);
                        setState(() {});
                      }
                    });
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Longitude'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _radius,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Radius (meter)'),
                  validator: validator,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(bottom: 12.0),
                  child: Row(children: [
                    Text('Kunci Lokasi'),
                    Padding(
                      padding: const EdgeInsets.only(left:50.0),
                      child: Switch(
                        focusColor: Colors.blue,
                        value: isSwitched,
                        onChanged: (value) {
                          setState(() {
                            isSwitched = value;
                            print(isSwitched);
                          });
                        },
                        activeTrackColor: Colors.lightBlue,
                        activeColor: Colors.blue,
                      ),
                    ),
                  ])),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: _isLoadingGetEmp == false ? multipleSelect() : null,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'SIMPAN',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    saveForm();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget multipleSelect() {
    return MultiSelect(
        autovalidate: true,
        initialValue: employeeSelected,
        titleText: 'Karyawan',
        validator: (dynamic value) {
          if (value == null) {
            return 'Silahkan pilih satu atau lebih';
          }
          return null;
        },
        errorText: 'Silahkan pilih satu atau lebih',
        dataSource: employeeList,
        textField: 'fullName',
        valueField: 'employeeId',
        filterable: true,
        onSaved: (value) {
          setState(() {
            print('The value is $value');
            employeeSelected = value;
          });
        });
  }
}
