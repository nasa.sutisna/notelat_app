import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notelat_app/src/blocs/worklocationBloc.dart';
import 'package:notelat_app/src/models/worklocationModel.dart';
import 'package:notelat_app/src/screens/screen_admin/worklocation/worklocationCreate.dart';
import 'package:notelat_app/src/widgets/menu_drawer.dart';

class WorklocationPage extends StatefulWidget {
  @override
  _WorklocationPageState createState() => _WorklocationPageState();
}

class _WorklocationPageState extends State<WorklocationPage> {
  List<DataWorklocation> listData = [];
  TextEditingController keyword;
  bool isSearching = false;
  Timer _debounce;

  @override
  void initState() {
    keyword = TextEditingController();
    worklocationBloc.getListData(1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: !isSearching
            ? Text('Lokasi Kerja')
            : TextField(
                autofocus: true,
                onChanged: (value) {
                  if (_debounce?.isActive ?? false) _debounce.cancel();
                  _debounce = Timer(const Duration(milliseconds: 500), () {
                    // do something with _searchQuery.text
                    worklocationBloc.getListData(1, keyword: value);
                  });
                },
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    hintText: "Cari...",
                    hintStyle: TextStyle(color: Colors.white)),
              ),
        actions: <Widget>[
          IconButton(
              icon: !isSearching ? Icon(Icons.search) : Icon(Icons.close),
              onPressed: () {
                setState(() {
                  isSearching = !isSearching;
                  if (!isSearching) {
                    worklocationBloc.getListData(1, keyword: '');
                  }
                });
              })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: StreamBuilder<WorklocationModel>(
            stream: worklocationBloc.listData,
            builder: (BuildContext context, AsyncSnapshot<WorklocationModel> snapshot) {
              if (snapshot.data == null) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return WorklocationList(
                  listData: snapshot.data.data,
                  totalPage: snapshot.data.totalPage,
                );
              }
            }),
      ),
      floatingActionButton: FloatingActionButton(
          elevation: 0.0,
          child: Icon(Icons.add),
          backgroundColor: Colors.lightBlue,
          onPressed: () {
            Navigator.of(context)
                .push(
                    MaterialPageRoute(builder: (context) => WorklocationCreatePage()))
                .then((value) {
              worklocationBloc.getListData(1);
            });
          }),
      drawer: MenuDrawer(),
    );
  }
}

class WorklocationList extends StatefulWidget {
  final listData;
  final totalPage;
  WorklocationList({Key key, this.listData, this.totalPage}) : super(key: key);

  @override
  _WorklocationListState createState() => _WorklocationListState();
}

class _WorklocationListState extends State<WorklocationList> {
  ScrollController scrollController = new ScrollController();
  List<DataWorklocation> listData;
  int currentPage = 1;

  getMoreData() {
    worklocationBloc.getListData(currentPage + 1, infinite: true).then((value) {
      final response = value;
      List data = response.data;
      currentPage = response.page;

      data.forEach((element) {
        setState(() {
          listData.add(element);
        });
      });
    });
  }

  @override
  void initState() {
    listData = widget.listData;
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (currentPage < widget.totalPage) {
          getMoreData();
        }
      }
    });
    super.initState();
  }

  @override
  void didUpdateWidget(WorklocationList oldWidget) {
    listData = widget.listData;
    currentPage = 1;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: listData.length,
          controller: scrollController,
          itemBuilder: (BuildContext context, int i) {
            return Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                child: Card(
                  elevation: 10,
                  child: listData.length > 0 ? InkWell(
                    onTap: () {
                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => WorklocationCreatePage(navParams: listData[i],))).then((value){
                       worklocationBloc.getListData(1);
                     });
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            listData[i].worklocationName,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.bold),
                          ),
                        
                        ],
                      ),
                    ),
                  ) : Container(),
                ));
          }),
    );
  }
}
