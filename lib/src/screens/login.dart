import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:notelat_app/src/blocs/loginBloc.dart';
import 'package:notelat_app/src/models/loginModel.dart';
import 'package:notelat_app/src/screens/home.dart';
import 'package:notelat_app/src/screens/screen_admin/employee/employee.dart';
import 'package:notelat_app/src/utills/notifView.dart';
import 'package:notelat_app/src/utills/responseView.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:toast/toast.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> implements ResponseView<LoginModel> {
  bool _isLoading = false;
  NotifView notif;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool passwordVisible = true;

  @override
  void onError(dynamic error) {
    setState(() {
      _isLoading = false;
    });

    Toast.show(error.message["message"], context, duration: Toast.LENGTH_LONG);
  }

  @override
  void onSuccess(LoginModel response) async {
    setState(() {
      _isLoading = false;
    });

    await StoragePreferences.setToken(response.data.accessToken);
    await StoragePreferences.setRole(response.data.role);
    await StoragePreferences.setUserData(response.data.employee);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => response.data.role == 1 ? EmployeePage() : Home()));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        // resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            Positioned(
                bottom: 0,
                right: 0,
                child: Image.asset(
                  "assets/bg-bawah.png",
                  width: size.width * 0.4,
                )),
            ModalProgressHUD(child: loginForm(), inAsyncCall: _isLoading),
            Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/bg-atas.png",
                  width: size.width * 0.25,
                ))
          ],
        ));
  }

  Widget loginForm() {
    return StreamBuilder<Object>(
        stream: loginBloc.loginFetcher,
        builder: (context, AsyncSnapshot snapshot) {
          return ListView(children: <Widget>[
            Container(
              color: Colors.white24,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 120.0),
              child: Column(
                children: [
                  Column(
                    children: <Widget>[
                      Text(
                        'Login',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 15.0),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: Image.asset(
                              "assets/image-login.png",
                              height: 150.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 16.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.blue[100],
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: TextField(
                          onChanged: loginBloc.email,
                          decoration: InputDecoration(
                              hintText: "Email",
                              prefixIcon: Icon(
                                Icons.email,
                                color: Colors.blue,
                              ),
                              border: InputBorder.none),
                        ),
                      )),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 8.0, right: 8.0, top: 16.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.blue[100],
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: TextField(
                        onChanged: loginBloc.password,
                        obscureText: passwordVisible,
                        decoration: InputDecoration(
                            hintText: 'Password',
                            border: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.lock,
                              color: Colors.blue,
                            ),
                            // Here is key idea
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                passwordVisible
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  passwordVisible = !passwordVisible;
                                });
                              },
                            )),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 20.0),
                    child: ButtonTheme(
                      height: 50.0,
                      minWidth: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        onPressed: () async {
                          setState(() {
                            _isLoading = true;
                          });

                          await loginBloc.doLogin(this);
                        },
                        child: Text(
                          'Login',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        color: Colors.blue,
                        splashColor: Colors.lightBlue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ]);
        });
  }
}
