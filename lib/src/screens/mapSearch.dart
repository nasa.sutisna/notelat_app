import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as location;
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';

const kGoogleApiKey = "AIzaSyB-g0OQTci6K6G5aIewkWtCblQ6GB6TIis"; //dev

// to get places detail (lat/lng)
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

final customTheme = ThemeData(
  primarySwatch: Colors.blue,
  brightness: Brightness.dark,
  accentColor: Colors.redAccent,
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.00)),
    ),
    contentPadding: EdgeInsets.symmetric(
      vertical: 12.50,
      horizontal: 10.00,
    ),
  ),
);

class MapSearch extends StatefulWidget {
  final latitude;
  final longitude;

  MapSearch({Key key, this.latitude, this.longitude}) : super(key: key);
  @override
  _MapSearchState createState() => _MapSearchState();
}

final homeScaffoldKey = GlobalKey<ScaffoldState>();
final searchScaffoldKey = GlobalKey<ScaffoldState>();

class _MapSearchState extends State<MapSearch> {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  Map<String, double> currentLocation;
  List<Marker> allMarkers = [];
  double latitude;
  double longitude;

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  void _updatePosition(CameraPosition _position) {
    print(
        'inside updatePosition ${_position.target.latitude} ${_position.target.longitude}');
    Marker marker = allMarkers.firstWhere(
        (p) => p.markerId == MarkerId('marker_2'),
        orElse: () => null);

    allMarkers.remove(marker);
    allMarkers.add(
      Marker(
        markerId: MarkerId('myMarker'),
        position: LatLng(_position.target.latitude, _position.target.longitude),
        draggable: true,
      ),
    );

    latitude = _position.target.latitude;
    longitude = _position.target.longitude;
    setState(() {});
  }

  void _getLocation() async {
    final locate = location.Location();
    var permission = await locate.hasPermission();

    if (permission != location.PermissionStatus.denied) {
      final location.LocationData pos = await locate.getLocation();
      print('widget ${widget.latitude.hashCode}');
      print('pos ${pos.latitude}');
      setState(() {
        latitude = widget.latitude.hashCode == 1
            ? pos.latitude
            : double.parse(widget.latitude);
        longitude = widget.longitude.hashCode == 1
            ? pos.longitude
            : double.parse(widget.longitude);
        currentLocation = {"lat": latitude, "lng": longitude};

        allMarkers.add(Marker(
            markerId: MarkerId('myMarker'),
            draggable: true,
            position: LatLng(latitude, longitude),
            onTap: () {
              print('marker tagged');
            }));
      });
    } else {
      var permission = await locate.requestPermission();
      print('denied permission $permission');
      _getLocation();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: homeScaffoldKey,
      body: SafeArea(
          child: Stack(
        children: <Widget>[
          latitude == null && longitude == null
              ? Container()
              : maps(_controller, allMarkers, latitude, longitude),
          Center(
            child: Container(
              width: 400,
              padding: EdgeInsets.only(top: 26.0),
              alignment: Alignment.topCenter,
              child: ButtonTheme(
                  minWidth: 350,
                  height: 50,
                  buttonColor: Colors.white,
                  child: RaisedButton.icon(
                      elevation: 10,
                      onPressed: () {
                        _handlePressButton();
                      },
                      icon: Icon(Icons.search),
                      label: Text("Cari alamat...",
                          style: TextStyle(fontSize: 16.0)))),
            ),
          ),
          Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.only(bottom: 26.0, left: 26),
                alignment: Alignment.bottomLeft,
                child: ButtonTheme(
                    minWidth: 100,
                    height: 50,
                    buttonColor: Colors.white,
                    child: RaisedButton.icon(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      color: Colors.red,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                      label: Text(
                        'Batal',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    )),
              )),
          Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: EdgeInsets.only(bottom: 26.0, right: 26),
                alignment: Alignment.bottomRight,
                child: ButtonTheme(
                    minWidth: 100,
                    height: 50,
                    buttonColor: Colors.white,
                    child: RaisedButton.icon(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      color: Colors.green,
                      onPressed: () {
                        print('latitude :$latitude - longitude :$longitude');
                        Navigator.of(context).pop({
                          "latitude": latitude.toString(),
                          "longitude": longitude.toString()
                        });
                      },
                      icon: Icon(
                        Icons.check,
                        color: Colors.white,
                      ),
                      label: Text(
                        'Pilih',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    )),
              )),
        ],
      )),
    );
  }

  Widget maps(_controller, allMarker, latitude, longitude) {
    return Container(
      child: GoogleMap(
        mapType: MapType.normal,
        cameraTargetBounds: CameraTargetBounds.unbounded,
        compassEnabled: true,
        initialCameraPosition: CameraPosition(
            target: LatLng(latitude ?? 0, longitude ?? 0), zoom: 17.4746),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          mapController = controller;
        },
        onCameraMove: ((_position) => _updatePosition(_position)),
        markers: Set.from(allMarker),
        zoomGesturesEnabled: true,
      ),
    );
  }

  void onError(PlacesAutocompleteResponse response) {
    print(response.errorMessage);
    // homeScaffoldKey.currentState.showSnackBar(
    //   SnackBar(content: Text(response.errorMessage)),
    // );
  }

  Future<void> _handlePressButton() async {
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "id",
      components: [Component(Component.country, "id")],
    );

    displayPrediction(p, homeScaffoldKey.currentState);
  }

  void moveCamera(lat, lon) {
    setState(() {
      latitude = lat;
      longitude = lon;
    });
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, lon), zoom: 20.0),
      ),
    );
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      moveCamera(lat, lng);

      setState(() {
        latitude = lat;
        longitude = lng;
      });
      print("${p.description} - $lat/$lng");
      scaffold.showSnackBar(
        SnackBar(content: Text("${p.description} - $lat/$lng")),
      );
    }
  }
}
