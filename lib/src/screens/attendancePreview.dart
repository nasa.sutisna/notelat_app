import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:notelat_app/src/blocs/attendanceBloc.dart';
import 'package:notelat_app/src/models/responseFaceRecognitionModel.dart';
import 'package:notelat_app/src/models/responseSuccess.dart';
import 'package:notelat_app/src/screens/cameraScreen.dart';
import 'package:notelat_app/src/utills/responseView.dart';
import 'package:notelat_app/src/utills/userData.dart';
import 'package:toast/toast.dart';

class AttendancePreview extends StatefulWidget {
  final geolocation;
  final photo;
  final employeeId;

  AttendancePreview({Key key, this.geolocation, this.photo, this.employeeId})
      : super(key: key);

  @override
  _AttendancePreviewState createState() => _AttendancePreviewState();
}

class _AttendancePreviewState extends State<AttendancePreview>
    implements ResponseView<ResponseSucessModel> {
  bool isLoading = false;
  bool isCheckFace = true;
  String filename = '';
  Future<ResponseFaceRecognitionModel> faceRecoginition;
  String faceRecogError;
  bool isFacerecogError = false;
  String hour = DateFormat.Hm().format(new DateTime.now());
  String date = DateFormat.yMMMMd().format(new DateTime.now());
  dynamic body;

  @override
  void initState() {
    initializeDateFormatting('in_ID');
    date = DateFormat.yMMMMd('in_ID').format(new DateTime.now());
    getFaceRecognition();
    super.initState();
  }

  @override
  void onError(message) {
    presentDialog(message);
    setState(() {
      this.isLoading = false;
    });
  }

  @override
  void didUpdateWidget(AttendancePreview oldWidget) {
    print('updatewirdeateatet');
    super.didUpdateWidget(oldWidget);
  }

  Future getFaceRecognition() async {
    final faceRecog =
        await attendanceBloc.faceRecognition(widget.photo, widget.employeeId);
    this.body = {
      "employeeId": widget.employeeId,
      "dateTime": new DateTime.now().toIso8601String(),
      "geolocation": widget.geolocation,
      "photo": faceRecog != null && faceRecog['filename'] != null
          ? faceRecog['filename']
          : null,
    };

    setState(() {});
    return faceRecog;
  }

  @override
  void onSuccess(data) async {
    setState(() {
      this.isLoading = false;
    });
    Toast.show(data.message, context, duration: Toast.LENGTH_LONG);
    Navigator.pop(context);
  }

  presentDialog(String message) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            title: Center(
                child: Text(
              message,
              style: TextStyle(fontSize: 16.0),
            )),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      // Navigator.pop(context);
                    },
                    child: Text(
                      "OK",
                      style: TextStyle(
                          fontWeight: FontWeight.w800, fontSize: 18.0),
                    ),
                  )
                ],
              )
            ],
          );
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Pratinjau'),
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: preview(),
            ),
          ],
        ));
  }

  Widget preview() {
    return Container(
      child: Column(
        children: <Widget>[
          imagePreview(),
          SizedBox(
            height: 50,
          ),
          Text(
            date,
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.black54),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            hour,
            style: TextStyle(fontSize: 35, fontWeight: FontWeight.w900),
          ),
          SizedBox(
            height: 30,
          ),
          checkFaceRecognition()
        ],
      ),
    );
  }

  Widget btnRecord(bool disabled) {
    return ButtonTheme(
      height: 45,
      minWidth: MediaQuery.of(context).size.width / 1.5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      child: MaterialButton(
          padding: EdgeInsets.all(10),
          color: Colors.blue,
          onPressed: this.isLoading || disabled
              ? null
              : () async {
                  setState(() {
                    this.isLoading = true;
                  });
                  await attendanceBloc.recordAttendance(this, this.body);
                },
          child: setButtonValue()),
    );
  }

  Widget setButtonValue() {
    if (this.isLoading == false) {
      return Text('Simpan Kehadiran',
          style: TextStyle(color: Colors.white, fontSize: 16.0));
    } else {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
      );
    }
  }

  Widget checkFaceRecognition() {
    return StreamBuilder(
        stream: attendanceBloc.faceRecognitionStream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            print('ada error ${snapshot.error}');
          }
          return Column(
            children: <Widget>[
              Center(
                  child: snapshot.connectionState == ConnectionState.waiting
                      ? Center(
                          child: Column(
                            children: <Widget>[
                              CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.blue),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 15),
                                child: Text('Memproses face recognition...'),
                              ),
                            ],
                          ),
                        )
                      : snapshot.hasData
                          ? Text(snapshot.data['message'])
                          : snapshot.hasError
                              ? Text(snapshot.error.toString())
                              : Text('server error')),
              SizedBox(
                height: 50,
              ),
              snapshot.hasData && snapshot.data['status'] == 200
                  ? btnRecord(
                      snapshot.connectionState == ConnectionState.waiting ||
                              snapshot.hasError
                          ? true
                          : false)
                  : btnRetake(
                      snapshot.connectionState == ConnectionState.waiting
                          ? false
                          : snapshot.hasError || snapshot.data['status'] == 400
                              ? true
                              : false),
            ],
          );
        });
  }

  Widget imagePreview() {
    return StreamBuilder(
        stream: attendanceBloc.faceRecognitionStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.only(top: 70.0),
              child: Center(
                child: CachedNetworkImage(
                  imageUrl: attendanceBloc.getImage(snapshot.data['filename']),
                  placeholder: (context, url) {
                    return Center(
                        child: Stack(
                      children: <Widget>[
                        Center(
                          child: Image.file(widget.photo,
                              height: 270.0, width: 180.0, fit: BoxFit.fill),
                        ),
                        Center(
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 110,
                              ),
                              CircularProgressIndicator(),
                            ],
                          ),
                        ),
                      ],
                    ));
                  },
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  width: 180.0,
                  fit: BoxFit.fill,
                ),
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.only(top: 70.0),
              child: Center(
                child: Image.file(widget.photo,
                    height: 270.0, width: 180.0, fit: BoxFit.fill),
              ),
            );
          }
        });
  }

  Widget btnRetake(bool enabled) {
    return ButtonTheme(
        height: 45,
        minWidth: MediaQuery.of(context).size.width / 1.5,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        child: MaterialButton(
            padding: EdgeInsets.all(10),
            color: Colors.deepOrange,
            onPressed: enabled
                ? () async {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CameraScreen(
                                  geolocation: widget.geolocation,
                                  employeeId: userData.employeeId,
                                )));
                    Navigator.pop(context);
                  }
                : null,
            child: Text('Ambil ulang foto',
                style: TextStyle(color: Colors.white, fontSize: 16.0))));
  }
}
