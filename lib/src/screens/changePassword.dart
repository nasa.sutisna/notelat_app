import 'package:flutter/material.dart';
import 'package:notelat_app/src/blocs/loginBloc.dart';
import 'package:notelat_app/src/widgets/menu_drawer.dart';
import 'package:toast/toast.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  TextEditingController _lastPassword, _newPassword, _confirmNewPassword;
  FocusNode _focusNode = new FocusNode();

  @override
  void initState() {
    _lastPassword = TextEditingController();
    _newPassword = TextEditingController();
    _confirmNewPassword = TextEditingController();
    super.initState();
  }

  void submit() async {
    // if (_formKey.currentState.validate()) {
    //   _formKey.currentState.save();
    //   try {
    //     var result = await loginBloc.changePassword(
    //         _newPassword.text, _lastPassword.text);
    //     print('resul $result');
    //     Toast.show("Ubah password berhasil", context, duration: Toast.LENGTH_LONG);
    //   } catch (error) {
    //     print('error tes $error');
    //     Toast.show(error['message'], context, duration: Toast.LENGTH_LONG);
    //   }
    // }
  }

  String validator(String value) {
    if (value.isEmpty)
      return "Tidak boleh kosong";
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Ubah Password'),
        ),
        body: formAction());
  }

  Widget formAction() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          autovalidate: _validate,
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _lastPassword,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.go,
                  obscureText: true,
                   focusNode: _focusNode,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Password Lama'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  controller: _newPassword,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.go,
                  focusNode: _focusNode,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Password Baru'),
                  validator: validator,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: TextFormField(
                  textInputAction: TextInputAction.go,
                  controller: _confirmNewPassword,
                  keyboardType: TextInputType.text,
                   focusNode: _focusNode,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Konfirmasi Password Baru'),
                  validator: validator,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'SIMPAN',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    _focusNode.unfocus();
                    submit();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
