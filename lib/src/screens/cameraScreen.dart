import 'dart:io';
import 'package:camera/camera.dart';
import 'package:exif/exif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:notelat_app/src/screens/attendancePreview.dart';
import 'package:notelat_app/src/utills/userData.dart';
import 'package:path_provider/path_provider.dart';

class CameraScreen extends StatefulWidget {
  final geolocation;
  final currentPhoto;
  final employeeId;

  const CameraScreen(
      {Key key, this.geolocation, this.currentPhoto, this.employeeId})
      : super(key: key);
  @override
  CameraScreenState createState() => CameraScreenState();
}

class CameraScreenState extends State<CameraScreen>
    with AutomaticKeepAliveClientMixin {
  CameraController _controller;
  List<CameraDescription> _cameras;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var imageUrl;

  @override
  void initState() {
    this._initCamera();
    print('geolocation ${widget.geolocation}');
    print('employeeId ${userData.employeeId}');
    setNullImage();
    super.initState();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  Future<void> _initCamera() async {
    _cameras = await availableCameras();
    _controller = CameraController(_cameras[1], ResolutionPreset.medium);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  setNullImage() {
    setState(() {
      this.imageUrl = null;
      deleteDir();
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (_controller != null) {
      if (!_controller.value.isInitialized) {
        return Container();
      }
    } else {
      return Center(
        child: SizedBox(
          height: 32,
          width: 32,
        ),
      );
    }

    if (!_controller.value.isInitialized) {
      return Container();
    }

    if (this.imageUrl == null) {
      return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        key: _scaffoldKey,
        extendBody: true,
        body: Stack(
          children: <Widget>[
            _buildCameraPreview(),
            _buildBottomNavigationBar()
          ],
        ),
      );
    } else {
      print('masuk preview');
      return Scaffold(key: _scaffoldKey, body: _previewImage(this.imageUrl));
    }
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildCameraPreview() {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: ClipRect(
      child: Container(
        child: Transform.scale(
          scale: _controller.value.aspectRatio / size.aspectRatio,
          child: Center(
            child: AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: CameraPreview(_controller),
            ),
          ),
        ),
      ),
    ));
  }

  String _timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  needRotation(String path) async {
    Map<String, IfdTag> data =
        await readExifFromBytes(await new File(path).readAsBytes());
    print('exif result');
    for (String key in data.keys) {
      print("$key (${data[key].tagType}): ${data[key]}");
    }
    return data;
  }

  Widget _buildBottomNavigationBar() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          // color: Theme.of(context).bottomAppBarColor,
          height: 100.0,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.white,
                radius: 28.0,
                child: IconButton(
                  icon: Icon(
                    Icons.camera_alt,
                    size: 28.0,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    _captureImage();
                  },
                ),
              )
            ],
          ),
        ));
  }

  Widget _previewImage(imageUrl) {
    return SafeArea(
        child: Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.file(
            imageUrl,
            fit: BoxFit.contain,
          ),
        ),
        Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: FloatingActionButton(
                heroTag: "btn1",
                onPressed: () {
                  setNullImage();
                },
                child: Icon(Icons.camera_alt),
              ),
            )),
        Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: FloatingActionButton(
                heroTag: "btn2",
                backgroundColor: Colors.green,
                onPressed: () {
                  gotoAttendancePreview();
                },
                child: Icon(Icons.check),
              ),
            )),
      ],
    ));
  }

  gotoAttendancePreview() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AttendancePreview(
                  geolocation: widget.geolocation,
                  photo: this.imageUrl,
                  employeeId: userData.employeeId,
                )));
    Navigator.pop(context);
  }

  void _captureImage() async {
    if (_controller.value.isInitialized) {
      // sound camera
      SystemSound.play(SystemSoundType.click);
      // get directory
      final Directory extDir = await getApplicationDocumentsDirectory();
      // set directory file
      final String dirPath = '${extDir.path}/media';
      // create directory in memory
      await Directory(dirPath).create(recursive: true);
      // file path
      final String filePath = '$dirPath/${_timestamp()}.jpeg';

      // take file with destination path
      await _controller.takePicture(filePath);
      final exif = needRotation(filePath);
      print('exif $exif');
      final imageUrl = await getLastImage(filePath);
      print('image before $imageUrl');
      setState(() {
        this.imageUrl = imageUrl;
      });
    }
  }

  Future<void> deleteDir() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    // path folder file
    final String dirPath = '${extDir.path}/media';
    // get folder directory
    final myDir = Directory(dirPath);
    myDir.deleteSync(recursive: true);
  }

  Future<FileSystemEntity> getLastImage(filePath) async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    // path folder file
    final String dirPath = '${extDir.path}/media';
    // get folder directory
    final myDir = Directory(dirPath);

    List<FileSystemEntity> _images;
    _images = myDir.listSync(recursive: true, followLinks: false);

    _images.sort((a, b) {
      return b.path.compareTo(a.path);
    });

    var lastFile = _images[0];
    print('last file : ${lastFile.uri}');
    // var pathLink = lastFile.resolveSymbolicLinksSync();
    // final tempName = new Random().nextInt(1000000000);
    // print('_images $_images');
    // print('pathLink $pathLink');

    // final dirTemp = extDir.absolute.path + '/media/$tempName.jpeg';
    // lastFile = await compressImage(lastFile);
    lastFile = await FlutterExifRotation.rotateImage(path: lastFile.path);

    // var extension = path.extension(lastFile.path);
    return lastFile;
  }
}
