import 'dart:io';

import 'package:dio/dio.dart';
import 'package:notelat_app/src/models/responseSuccess.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'dart:async';
import 'dart:convert';
import 'package:notelat_app/src/utills/storagePreferences.dart';

class AttendanceApi {
  Dio dio = new Dio();

  final String _url = configApi.baseUrl;
  final headers = configApi.headers();
  get message => null;

  Future recordAttendance(data) async {
    print(data);
    try {
      final response = await dio.post("$_url/attendance/recordAttendance",
          data: data, options: await headers);
      return ResponseSucessModel.fromJson(response.data);
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future faceRecognition(File file, String employeeId) async {
    String filename = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(file.path, filename: filename),
      "employeeId": employeeId
    });

    try {
      final response = await dio.post("$_url/attendance/faceRecognition",
          data: formData, options: await headers);
      return json.decode(json.encode(response.data));
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future getTodayAttendance() async {
    try {
      final userData = await StoragePreferences.getUserData();
      final token = await StoragePreferences.getToken();
      final employeeId = userData["employeeId"];

      final response = await dio.post("$_url/attendance/getTodayAttendance",
          data: {'employeeId': employeeId},
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({message: 'server error'});
      }
    }
  }

  Future checkFirstFoto() async {
    try {
      final userData = await StoragePreferences.getUserData();
      final employeeId = userData["employeeId"];
      final token = await StoragePreferences.getToken();

      final response = await dio.get("$_url/attendance/checkFirstFace/$employeeId",
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({message: 'server error'});
      }
    }
  }

}
