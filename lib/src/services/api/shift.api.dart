import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:notelat_app/src/models/shiftModel.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'dart:async';
import 'package:notelat_app/src/utills/storagePreferences.dart';

class ShiftApi {
  Dio dio = new Dio();

  final String _url = '${configApi.baseUrl}/shift';
  Future headers = configApi.headers();

  Future<ShiftModel> getListShift(page, limit, keyword) async {
    try {
      final token = await StoragePreferences.getToken();
      final response =
          await dio.get("$_url?page=$page&limit=$limit&keyword=$keyword",
              options: Options(
                headers: {"Authorization": "$token"},
              ));

      return ShiftModel.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future getListShiftGroup(page, limit, keyword) async {
    try {
      final token = await StoragePreferences.getToken();
      final response = await dio.get(
          "$_url/shiftGroup?page=$page&limit=$limit&keyword=$keyword",
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future getShiftDaily(shiftGroupCode) async {
    try {
      final token = await StoragePreferences.getToken();
      final response = await dio.get(
          "$_url/getDailyShift?shiftGroupCode=$shiftGroupCode",
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future addShift(payload) async {
    try {
      final response = await dio.post("$_url/addShift",
          data: payload, options: await headers);
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future addShiftGroup(payload) async {
    try {
      final response = await dio.post("$_url/addShiftGroup",
          data: payload, options: await headers);
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future editShiftGroup(payload) async {
    try {
      final response = await dio.post("$_url/editShiftGroup",
          data: payload, options: await headers);
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future editShift(payload) async {
    try {
      final response = await dio.post("$_url/editShift",
          data: payload, options: await headers);
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }
}
