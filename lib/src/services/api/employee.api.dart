import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'dart:async';

import 'package:notelat_app/src/utills/storagePreferences.dart';

class EmployeeApi {
  Dio dio = new Dio();

  final String _url = '${configApi.baseUrl}/employee';
  Future headers = configApi.headers();

  Future getEmployees(page, limit, keyword, module) async {
    try {
      final token = await StoragePreferences.getToken();
      final response = await dio.get(
          "$_url?page=$page&limit=$limit&keyword=$keyword&module=$module",
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future getDetailEmployee(String empId) async {
    try {
      final token = await StoragePreferences.getToken();
      final response = await dio.get("$_url?employeeId=$empId",
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future addEmployee(File file, dynamic data) async {
    print(data);
    String filename = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(file.path, filename: filename),
      "fullName": data['fullName'],
      "employeeNumber": data['employeeNumber'],
      "email": data['email'],
      "phoneNumber": data['phoneNumber'],
      "position": data['position']
    });

    try {
      final response =
          await dio.post("$_url/add", data: formData, options: await headers);
      print('save 1 ${response.data}');
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future editEmployee(File file, dynamic data) async {
    print('file $file');
    print('payload ${data['employeeId']}');
    String filename = file != null ? file.path.split('/').last : '';

    print('filename $filename');
    FormData formData = FormData.fromMap({
      "file": file != null
          ? await MultipartFile.fromFile(file.path, filename: filename)
          : null,
      "employeeId": data['employeeId'],
      "employeeNumber": data['employeeNumber'],
      "fullName": data['fullName'],
      "email": data['email'],
      "phoneNumber": data['phoneNumber'],
      "position": data['position'],
      "deleted": data['deleted'] ?? 0
    });

    try {
      final response = await dio.post("$_url/update",
          data: formData, options: await headers);
      return response.data;
    } on DioError catch (error) {
      print('error $error');
      throw responseError.getMessage(error);
    }
  }
}
