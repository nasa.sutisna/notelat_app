import 'package:dio/dio.dart';
import 'dart:async';

import 'package:notelat_app/src/models/loginModel.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';

class LoginApi {
  final String _url = configApi.baseUrl;
  Dio dio = new Dio();
  get message => null;

  Future doLogin(email, password) async {
    try {
      print('email $email password $password');
      if (email == null && password == null) {
        throw Exception({"message": 'Email dan password harus diisi'});
      } else if (email == null) {
        throw Exception({"message": 'Email harus diisi'});
      } else if (password == null) {
        throw Exception({"message": 'Password harus diisi'});
      }

      final response = await dio.post("$_url/auth/doLogin",
          data: {'email': email, 'password': password});
      return LoginModel.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future doLogout(token) async {
    try {
      await dio.post("$_url/auth/logout",
          options: Options(headers: {"Authorization": token}));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({message: 'server error'});
      }
    }
  }

  Future checkToken(token) async {
    try {
      await dio.get("$_url/auth/checkToken",
          options: Options(headers: {"Authorization": token}));
    } on DioError catch (e) {
      if (e.response != null) {
        throw e.response.data;
      } else {
        throw {message: 'server error'};
      }
    }
  }

  Future changePassword(String newPass, String oldPass) async {
    try {
      final userData = await StoragePreferences.getUserData();
      final token = await StoragePreferences.getToken();
      final email = userData["email"];
      final Map body = {
        'email': email,
        'newPassword': newPass,
        'oldPassword': oldPass
      };

      await dio.post("$_url/auth/changePassword",
          data: body, options: Options(headers: {"Authorization": token}));
    } on DioError catch (e) {
      if (e.response != null) {
        throw e.response.data;
      } else {
        throw Exception({message: 'server error'});
      }
    }
  }
}
