import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:notelat_app/src/models/worklocationModel.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'dart:async';
import 'package:notelat_app/src/utills/storagePreferences.dart';

class WorklocationApi {
  Dio dio = new Dio();

  final String _url = '${configApi.baseUrl}/worklocation';
  Future<WorklocationModel> getListData(page, limit, keyword) async {
    try {
      final token = await StoragePreferences.getToken();
      final response =
          await dio.get("$_url?page=$page&limit=$limit&keyword=$keyword",
              options: Options(
                headers: {"Authorization": "$token"},
              ));

      return WorklocationModel.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future getWorklocationByEmp(empId) async {
    try {
      final token = await StoragePreferences.getToken();
      final response =
          await dio.get("$_url/getWorklocationEmp?employeeId=$empId",
              options: Options(
                headers: {"Authorization": "$token"},
              ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future detail(id) async {
    try {
      final token = await StoragePreferences.getToken();
      final response = await dio.get("$_url/detail?id=$id",
          options: Options(
            headers: {"Authorization": "$token"},
          ));

      return jsonDecode(jsonEncode(response.data));
    } on DioError catch (e) {
      if (e.response != null) {
        throw Exception(e.response.data);
      } else {
        print('masuk sini $e');
        throw Exception({"message": 'server error'});
      }
    }
  }

  Future add(payload) async {
    try {
      final headers = await configApi.headers();
      final response =
          await dio.post("$_url/add", data: payload, options: headers);
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future edit(payload) async {
    try {
      final headers = await configApi.headers();
      final response =
          await dio.post("$_url/edit", data: payload, options: headers);
      return response.data;
    } on DioError catch (error) {
      throw responseError.getMessage(error);
    }
  }
}
