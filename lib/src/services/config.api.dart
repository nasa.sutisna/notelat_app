import 'package:dio/dio.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';

class ConfigApi {
  final baseUrl = 'https://62a38c3c17fc.ngrok.io';

  headers() async {
    final token = await StoragePreferences.getToken();
    final options = Options(
      headers: {"Authorization": "$token"},
    );

    return options;
  }
}

final configApi = new ConfigApi();
