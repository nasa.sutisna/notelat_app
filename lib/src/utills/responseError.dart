// To parse this JSON data, do
//
//     final errorHelper = errorHelperFromJson(jsonString);

class ResponseErrorHelper {
  String message;

  ResponseErrorHelper({
    this.message,
  });

  getMessage(error) {
    if (error.response != null) {
      return error.response.data["message"];
    } else {
      return 'server error';
    }
  }
}

final responseError = ResponseErrorHelper();
