import 'dart:math';

class Haversine {
  // diameter bumi dalam kilometer
  static final R = 6372.1;

  // rumus haversine formula
  static double haversine(double lat1, lon1, lat2, lon2) {
    double dLat = _toRadians(lat2 - lat1);
    double dLon = _toRadians(lon2 - lon1);
    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    double a =
        pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2);
    double c = 2 * asin(sqrt(a));
    return R * c;
  }

  // membuat diameter geofencing
  static double _toRadians(double degree) {
    return degree * pi / 180;
  }

  // check apakah lokasi terakhir user masih di dalam radius
  static dynamic inCoverage(
      dynamic geolocation, dynamic currentLocation, double radius) {
    double distance = Haversine.haversine(geolocation["lat"],
        geolocation["lng"], currentLocation["lat"], currentLocation["lng"]);
    double distanceMeter = distance * 1000;

    if (distanceMeter > radius) {
      return {"status": false, "distance": distanceMeter};
    } else {
      return {"status": true, "distance": distanceMeter};
    }
  }
}

final haversine = Haversine();
