import 'package:notelat_app/src/utills/storagePreferences.dart';

class UserDataPreferences {
  String employeeId = '';
  String photo = '';
  String userId = '';
  dynamic userData;

  getUserData() async {
    this.userData = await StoragePreferences.getUserData();
    print('userData $userData');
    this.employeeId = this.userData["employeeId"] ?? '';
    print('home employeeId $employeeId');
    this.photo = this.userData["photo"] ?? '';
    this.userId = this.userData["userId"] ?? '';
  }
}

final userData = new UserDataPreferences();