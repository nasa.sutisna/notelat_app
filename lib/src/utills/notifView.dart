import 'package:flutter/material.dart';

class NotifView {
  showSnackBar(_scaffoldKey, String text) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }
}
