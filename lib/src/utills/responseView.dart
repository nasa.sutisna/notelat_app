abstract class ResponseView <T>{
  void onSuccess(T data);
  void onError(dynamic message);
}
