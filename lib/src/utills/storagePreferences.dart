import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class StoragePreferences {
  static setToken(String token) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("token", token);
  }

  static setRole(int role) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt("role", role);
  }

  static getRole() async {
    var prefs = await SharedPreferences.getInstance();
    print('role $prefs');
    return prefs.getInt("role");
  }

  static getToken() async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.getString("token");
  }

  static setUserData(dynamic user) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("user", jsonEncode(user));
  }

  static getUserData() async {
    var prefs = await SharedPreferences.getInstance();
    var user = jsonDecode(prefs.getString("user"));
    return user;
  }

  static setWorklocation(dynamic worklocation) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("worklocation", jsonEncode(worklocation));
  }

  static getWorklocation() async {
    var prefs = await SharedPreferences.getInstance();
    var worklocation = jsonDecode(prefs.getString("worklocation"));
    return worklocation;
  }

  static clearStorage() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}
