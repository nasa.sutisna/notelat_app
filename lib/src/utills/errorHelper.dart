// To parse this JSON data, do
//
//     final errorHelper = errorHelperFromJson(jsonString);

import 'dart:convert';

ErrorHelper errorHelperFromJson(String str) => ErrorHelper.fromJson(json.decode(str));

String errorHelperToJson(ErrorHelper data) => json.encode(data.toJson());

class ErrorHelper {
    String message;

    ErrorHelper({
        this.message,
    });

    factory ErrorHelper.fromJson(Map<String, dynamic> json) => ErrorHelper(
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
    };
}
