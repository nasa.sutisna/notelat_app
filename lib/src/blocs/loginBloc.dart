import 'package:flutter/services.dart';
import 'package:notelat_app/src/models/loginModel.dart';
import 'package:notelat_app/src/resources/loginRepository.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'package:notelat_app/src/utills/responseView.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc {
  ResponseView<LoginModel> _response;

  final _repository = LoginRepository();
  final _loginFetcher = PublishSubject<void>();
  final _logoutFetcher = PublishSubject<void>();

  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  Function(String) get email => _email.sink.add;
  Function(String) get password => _password.sink.add;

  Stream<void> get loginFetcher => _loginFetcher.stream;
  Stream<void> get logoutFetcher => _logoutFetcher.stream;

  doLogin(ResponseView response) async {
    _response = response;
    _loginFetcher.sink.add(null);

    try {
      LoginModel doLogin =
          await _repository.doLogin(_email.value, _password.value);

      _response?.onSuccess(doLogin);
      _loginFetcher.sink.add(doLogin);
    } catch (error) {
      print('error pesan $error');
      _response?.onError(error);
      _loginFetcher.sink.addError(error);
    }
  }

  doLogout(token) async {
    try {
      final doLogout = await _repository.doLogout(token);
      return doLogout;
    } catch (error) {
      throw error;
    }
  }

  checkToken(token) async {
    try {
      final checkToken = await _repository.checkToken(token);
      return checkToken;
    } catch (error) {
      throw error;
    }
  }

   Future changePassword(String newPass, String oldPass) async {
    try {
      final save = await _repository.changePassword(newPass, oldPass);
      return save;
    } catch (error) {
      print('err $error');
      throw error;
    }
  }


  dispose() {
    _loginFetcher.close();
    _logoutFetcher.close();
    _email.close();
    _password.close();
  }
}

final loginBloc = LoginBloc();
