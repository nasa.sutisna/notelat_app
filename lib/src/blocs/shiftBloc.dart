import 'package:notelat_app/src/models/shiftModel.dart';
import 'package:notelat_app/src/resources/shiftRepository.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'package:rxdart/rxdart.dart';

class ShiftBloc {
  final _repository = ShiftRepository();
  final _listDataFetcher = PublishSubject<ShiftModel>();
  final _listDataShifGroupFetcher = PublishSubject<dynamic>();

  Stream<ShiftModel> get listData => _listDataFetcher.stream;
  Stream<dynamic> get listDataShifGroup => _listDataShifGroupFetcher.stream;

  Future<ShiftModel> getListShift(page, { int limit = 10, bool infinite = false, String keyword = ''}) async {
    try {
      final ShiftModel result = await _repository.getListShift(page, limit, keyword);

      if (infinite == false) {
        _listDataFetcher.sink.add(result);
      }

      return result;
    } catch (error) {
      print('error bloc $error');
      _listDataFetcher.sink.addError(error);
      throw error;
    }
  }

  Future getListShiftGroup(page, {bool infinite = false, String keyword = ''}) async {
    try {
      final result = await _repository.getListShiftGroup(page, 10, keyword);

      if (infinite == false) {
        _listDataShifGroupFetcher.sink.add(result);
      }

      return result;
    } catch (error) {
      _listDataShifGroupFetcher.sink.addError(error);
      throw error;
    }
  }

  Future getShiftDaily(shiftGroupCode) async {
    try {
      final result = await _repository.getShiftDaily(shiftGroupCode);
      return result;
    } catch (error) {
      throw error;
    }
  }

  Future addShift(data) async {
    try {
      final save = await _repository.addShift(data);
      return save;
    } catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future addShiftGroup(data) async {
    try {
      final save = await _repository.addShiftGroup(data);
      return save;
    } catch (error) {
      throw error;
    }
  }

  Future editShiftGroup(data) async {
    try {
      final save = await _repository.editShiftGroup(data);
      return save;
    } catch (error) {
      throw error;
    }
  }

  Future editShift(data) async {
    try {
      final save = await _repository.editShift(data);
      return save;
    } catch (error) {
      throw responseError.getMessage(error);
    }
  }

  dispose() {
    _listDataShifGroupFetcher.close();
    _listDataFetcher.close();
  }
}

final shiftBloc = ShiftBloc();
