import 'dart:io';
import 'package:notelat_app/src/resources/employeeRepository.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:rxdart/rxdart.dart';

class EmployeeBloc {
  final _repository = EmployeeRepository();
  final _dataEmployeesFetcher = PublishSubject<dynamic>();

  Stream<dynamic> get dataEmployees => _dataEmployeesFetcher.stream;

  Future addEmployee(File file, data) async {
    try {
      final save = await _repository.addEmployee(file, data);
      return save;
    } catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future editEmployee(File file, data) async {
    try {
      final save = await _repository.editEmployee(file, data);
      return save;
    } catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future getEmployees(page,
      {bool infinite = false,
      String keyword = '',
      int limit = 10,
      String module = ''}) async {
    try {
      final result =
          await _repository.getEmployees(page, limit, keyword, module);

      if (infinite == false) {
        _dataEmployeesFetcher.sink.add(result);
      }

      return result;
    } catch (error) {
      _dataEmployeesFetcher.sink.addError(error);
      throw error;
    }
  }

  Future<String> getImageProfile(filename) async {
    final token = await StoragePreferences.getToken();
    return configApi.baseUrl +
        '/employee/showimage/$filename?access_token=$token';
  }

  dispose() {
    _dataEmployeesFetcher.close();
  }
}

final employeeBloc = EmployeeBloc();
