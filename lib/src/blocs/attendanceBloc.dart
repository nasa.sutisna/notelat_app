import 'dart:io';
import 'package:notelat_app/src/resources/attendanceRepository.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/responseView.dart';
import 'package:rxdart/rxdart.dart';

class AttendanceBloc {
  final _repository = AttendanceRepository();
  final _todoFetcher = PublishSubject();
  final _recordFetcher = PublishSubject<dynamic>();
  final _faceRecogFetcher = PublishSubject();
  final _dataAttendanceFetcher = PublishSubject<dynamic>();

  Stream<dynamic> get dataAttendanceList => _dataAttendanceFetcher.stream;

  Stream get responseTodayAttendace => _todoFetcher.stream;
  Stream<dynamic> get recordProcess => _recordFetcher.stream;
  Stream get faceRecognitionStream => _faceRecogFetcher.stream;

  getTodayAttendance() async {
    try {
      final result = await _repository.getTodayAttendance();
      _todoFetcher.sink.add(result['data']);
    } catch (error) {
      _todoFetcher.sink.addError(error);
    }
  }

  Future recordAttendance(ResponseView response, data) async {
    try {
      print('body $data');
      final record = await _repository.recordAttendance(data);
      response?.onSuccess(record);
    } catch (error) {
      response?.onError(error);
    }
  }

  Future faceRecognition(File file, data) async {
    try {
      final record = await _repository.faceRecognition(file, data);
      _faceRecogFetcher.sink.add(record);
      return record;
    } catch (error) {
      _faceRecogFetcher.sink.addError(error);
    }
  }

   Future checkFirstFoto() async {
    try {
      final result = await _repository.checkFirstFoto();
      return result;
    } catch (error) {
      throw error;
    }
  }

  getImage(filename) {
    return configApi.baseUrl + '/attendance/showimage/$filename';
  }

  dispose() {
    _todoFetcher.close();
    _recordFetcher.close();
    _faceRecogFetcher.close();
    _dataAttendanceFetcher.close();
  }
}

final attendanceBloc = AttendanceBloc();
