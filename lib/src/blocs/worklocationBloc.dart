import 'package:notelat_app/src/models/worklocationModel.dart';
import 'package:notelat_app/src/resources/worklocationRepository.dart';
import 'package:notelat_app/src/utills/responseError.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:rxdart/rxdart.dart';

class WorklocationBloc {
  final _repository = WorklocationRepository();
  final _listDataFetcher = PublishSubject<WorklocationModel>();
  final _listDataByEmpFetcher = PublishSubject();
  final _detailFetcher = PublishSubject();

  Stream get listData => _listDataFetcher.stream;
  Stream<WorklocationModel> get listDataByEmp => _listDataByEmpFetcher.stream;
  Stream get detailFetcher => _detailFetcher.stream;

  Future<WorklocationModel> getListData(page,
      {int limit = 10, bool infinite = false, String keyword = ''}) async {
    try {
      final WorklocationModel result =
          await _repository.getListData(page, limit, keyword);

      if (infinite == false) {
        _listDataFetcher.sink.add(result);
      }

      return result;
    } catch (error) {
      print('error bloc $error');
      _listDataFetcher.sink.addError(error);
      throw error;
    }
  }

  Future getWorklocationByEmp(String empId) async {
    try {
      final result = await _repository.getWorklocationByEmp(empId);
      _listDataByEmpFetcher.sink.add(result['data']);
      StoragePreferences.setWorklocation(result['data']['worklocation']);
      return result['data'];
    } catch (error) {
      print('error bloc $error');
      _listDataByEmpFetcher.sink.addError(error);
      throw error;
    }
  }

  Future detail(id) async {
    try {
      final result = await _repository.detail(id);
      _detailFetcher.sink.add(result);
      return result;
    } catch (error) {
      print('error bloc $error');
      _detailFetcher.sink.addError(error);
      throw error;
    }
  }

  Future add(data) async {
    try {
      final save = await _repository.add(data);
      return save;
    } catch (error) {
      throw responseError.getMessage(error);
    }
  }

  Future edit(data) async {
    try {
      final save = await _repository.edit(data);
      return save;
    } catch (error) {
      throw responseError.getMessage(error);
    }
  }

  dispose() {
    _detailFetcher.close();
    _listDataFetcher.close();
    _listDataByEmpFetcher.close();
  }
}

final worklocationBloc = WorklocationBloc();
