import 'package:notelat_app/src/models/attendanceModel.dart';
import 'package:notelat_app/src/resources/homeRepository.dart';
import 'package:notelat_app/src/services/config.api.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final _repository = HomeRepository();
  final _todoFetcher = PublishSubject<AttendanceModel>();
  final _userData = PublishSubject<dynamic>();

  Stream<AttendanceModel> get allTodo => _todoFetcher.stream;
  Stream<dynamic> get userData => _userData.stream;

  getTodayAttendance() async {
    AttendanceModel todo = await _repository.getTodayAttendance();
    _todoFetcher.sink.add(todo);
  }

  Future getUserData() async {
    var user = await StoragePreferences.getUserData();
    print('userdata $user');
    return user;
  }

  Future getRole() async {
    var role = await StoragePreferences.getRole();
    print('role $role');
    return role;
  }

  showImageProfile(filename) async{
    var token = await StoragePreferences.getToken();
    var pathUrl =  configApi.baseUrl + '/employee/showimage/$filename?access_token=$token';
    print('path $pathUrl');
    return pathUrl;
  }

  dispose() {
    _todoFetcher.close();
    _userData.close();
  }
}

final homeBloc = HomeBloc();
