import 'dart:io';
import 'package:notelat_app/src/services/api/employee.api.dart';

class EmployeeRepository {
  final employeeApi = EmployeeApi();

  Future addEmployee(File file, body) => employeeApi.addEmployee(file, body);
  Future editEmployee(File file, body) => employeeApi.editEmployee(file, body);
  Future<dynamic> getEmployees(page, limit, keyword, module) =>
      employeeApi.getEmployees(page, limit, keyword, module);
}
