import 'dart:io';
import 'package:notelat_app/src/services/api/attendance.api.dart';

class AttendanceRepository {
  final attendanceApi = AttendanceApi();

  Future getTodayAttendance() => attendanceApi.getTodayAttendance();
  Future checkFirstFoto() => attendanceApi.checkFirstFoto();
  Future recordAttendance(data) => attendanceApi.recordAttendance(data);
  Future faceRecognition(File file, employeeId) =>
      attendanceApi.faceRecognition(file, employeeId);
}
