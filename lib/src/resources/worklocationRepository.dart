
import 'package:notelat_app/src/models/worklocationModel.dart';
import 'package:notelat_app/src/services/api/worklocation.api.dart';

class WorklocationRepository {
  final worklocation = WorklocationApi();

  Future<WorklocationModel> getListData(page,limit,keyword) => worklocation.getListData(page, limit,keyword);
  Future getWorklocationByEmp(String empId) => worklocation.getWorklocationByEmp(empId);
  Future detail(id) => worklocation.detail(id);
  Future add(payload) => worklocation.add(payload);
  Future edit(payload) => worklocation.edit(payload);
}
