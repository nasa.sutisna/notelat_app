import 'package:notelat_app/src/services/api/login.api.dart';

class LoginRepository{
  final loginApiProvider = LoginApi();

  Future doLogin(String email, String password) => loginApiProvider.doLogin(email, password);
  Future doLogout(String token) => loginApiProvider.doLogout(token);
  Future checkToken(String token) => loginApiProvider.checkToken(token);
  Future changePassword(String newPass, String oldPass) => loginApiProvider.changePassword(newPass, oldPass);
}