import 'package:notelat_app/src/models/shiftModel.dart';
import 'package:notelat_app/src/services/api/shift.api.dart';

class ShiftRepository {
  final shiftApi = ShiftApi();

  Future<ShiftModel> getListShift(page,limit,keyword) => shiftApi.getListShift(page, limit,keyword);
  Future<dynamic> getListShiftGroup(page,limit,keyword) => shiftApi.getListShiftGroup(page, limit,keyword);
  Future<dynamic> getShiftDaily(code) => shiftApi.getShiftDaily(code);
  Future<dynamic> addShift(payload) => shiftApi.addShift(payload);
  Future<dynamic> addShiftGroup(payload) => shiftApi.addShiftGroup(payload);
  Future<dynamic> editShiftGroup(payload) => shiftApi.editShiftGroup(payload);
  Future<dynamic> editShift(payload) => shiftApi.editShift(payload);
}
