import 'package:notelat_app/src/services/api/attendance.api.dart';

class HomeRepository{
  final attendanceApi = AttendanceApi();

  Future getTodayAttendance() => attendanceApi.getTodayAttendance();
}