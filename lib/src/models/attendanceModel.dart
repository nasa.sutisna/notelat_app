// To parse this JSON data, do
//
//     final attendanceModel = attendanceModelFromJson(jsonString);

import 'dart:convert';

AttendanceModel attendanceModelFromJson(String str) =>
    AttendanceModel.fromJson(json.decode(str));

String attendanceModelToJson(AttendanceModel data) =>
    json.encode(data.toJson());

class AttendanceModel {
  String message;
  Data data;

  AttendanceModel({
    this.message,
    this.data,
  });

  factory AttendanceModel.fromJson(Map<String, dynamic> json) =>
      AttendanceModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Attendance attendance;
  ShiftDaily shiftDaily;

  Data({
    this.attendance,
    this.shiftDaily,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        attendance: Attendance.fromJson(json["attendance"]),
        shiftDaily: ShiftDaily.fromJson(json["shiftDaily"]),
      );

  Map<String, dynamic> toJson() => {
        "attendance": attendance.toJson(),
        "shiftDaily": shiftDaily.toJson(),
      };
}

class Attendance {
  int attendaceId;
  String employeeId;
  String shiftGroupCode;
  DateTime startTime;
  DateTime endTime;
  dynamic startTimeLatlong;
  String endTimeLatlong;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;

  Attendance({
    this.attendaceId,
    this.employeeId,
    this.shiftGroupCode,
    this.startTime,
    this.endTime,
    this.startTimeLatlong,
    this.endTimeLatlong,
    this.photo,
    this.createdAt,
    this.updatedAt,
  });

  factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        startTime: DateTime.parse(json["startTime"]) ?? '',
        endTime:  DateTime.parse(json["endTime"]) ?? '',
        startTimeLatlong: json["startTimeLatlong"] ?? '',
        endTimeLatlong: json["endTimeLatlong"] ?? '',
        photo:  json["photo"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "startTime": startTime.toIso8601String() ?? '',
        "endTime": endTime.toIso8601String() ?? '',
        "startTimeLatlong": startTimeLatlong ?? '',
        "endTimeLatlong": endTimeLatlong ?? '',
        "photo": photo ?? ''
      };
}

class ShiftDaily {
  String shiftName;
  String shiftTime;

  ShiftDaily({
    this.shiftName,
    this.shiftTime,
  });

  factory ShiftDaily.fromJson(Map<String, dynamic> json) => ShiftDaily(
        shiftName: json["shiftName"] ?? '',
        shiftTime: json["shiftTime"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "shiftName": shiftName ?? '',
        "shiftTime": shiftTime ?? '',
      };
}
