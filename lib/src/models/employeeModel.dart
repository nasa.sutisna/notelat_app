// To parse this JSON data, do
//
//     final employeeModel = employeeModelFromJson(jsonString);

import 'dart:convert';

EmployeeModel employeeModelFromJson(String str) =>
    EmployeeModel.fromJson(json.decode(str));

String employeeModelToJson(EmployeeModel data) => json.encode(data.toJson());

class EmployeeModel {
  int page;
  int limit;
  int total;
  int totalPage;
  List<Datum> data;

  EmployeeModel({
    this.page,
    this.limit,
    this.total,
    this.totalPage,
    this.data,
  });

  factory EmployeeModel.fromJson(Map<String, dynamic> json) => EmployeeModel(
        page: json["page"],
        limit: json["limit"],
        total: json["total"],
        totalPage: json["totalPage"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "limit": limit,
        "total": total,
        "totalPage": totalPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  String employeeId;
  String fullName;
  String employeeNumber;
  String email;
  String phoneNumber;
  String position;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;

  Datum({
    this.employeeId,
    this.fullName,
    this.employeeNumber,
    this.email,
    this.phoneNumber,
    this.position,
    this.photo,
    this.createdAt,
    this.updatedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        employeeId: json["employeeId"],
        fullName: json["fullName"],
        employeeNumber:
            json["employeeNumber"] == null ? null : json["employeeNumber"],
        email: json["email"],
        phoneNumber: json["phoneNumber"],
        position: json["position"],
        photo: json["photo"] == null ? null : json["photo"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "employeeId": employeeId,
        "fullName": fullName,
        "employeeNumber": employeeNumber == null ? null : employeeNumber,
        "email": email,
        "phoneNumber": phoneNumber,
        "position": position,
        "photo": photo == null ? null : photo,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
      };
}
