// To parse this JSON data, do
//
//     final worklocationModel = worklocationModelFromJson(jsonString);

import 'dart:convert';

WorklocationModel worklocationModelFromJson(String str) => WorklocationModel.fromJson(json.decode(str));

String worklocationModelToJson(WorklocationModel data) => json.encode(data.toJson());

class WorklocationModel {
    int page;
    int limit;
    int total;
    int totalPage;
    List<DataWorklocation> data;

    WorklocationModel({
        this.page,
        this.limit,
        this.total,
        this.totalPage,
        this.data,
    });

    factory WorklocationModel.fromJson(Map<String, dynamic> json) => WorklocationModel(
        page: json["page"],
        limit: json["limit"],
        total: json["total"],
        totalPage: json["totalPage"],
        data: List<DataWorklocation>.from(json["data"].map((x) => DataWorklocation.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "page": page,
        "limit": limit,
        "total": total,
        "totalPage": totalPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class DataWorklocation {
    int worklocationId;
    String worklocationName;
    String latitude;
    String longitude;
    int radius;
    DateTime createdAt;
    DateTime updatedAt;

    DataWorklocation({
        this.worklocationId,
        this.worklocationName,
        this.latitude,
        this.longitude,
        this.radius,
        this.createdAt,
        this.updatedAt,
    });

    factory DataWorklocation.fromJson(Map<String, dynamic> json) => DataWorklocation(
        worklocationId: json["worklocationId"],
        worklocationName: json["worklocationName"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "worklocationId": worklocationId,
        "worklocationName": worklocationName,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
    };
}
