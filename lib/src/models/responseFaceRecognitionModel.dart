// To parse this JSON data, do
//
//     final responseFaceRecognitionModel = responseFaceRecognitionModelFromJson(jsonString);

import 'dart:convert';

ResponseFaceRecognitionModel responseFaceRecognitionModelFromJson(String str) =>
    ResponseFaceRecognitionModel.fromJson(json.decode(str));

String responseFaceRecognitionModelToJson(ResponseFaceRecognitionModel data) =>
    json.encode(data.toJson());

class ResponseFaceRecognitionModel {
  String message;
  String filename;
  double accuration;

  ResponseFaceRecognitionModel({
    this.message,
    this.filename,
    this.accuration,
  });

  factory ResponseFaceRecognitionModel.fromJson(Map<String, dynamic> json) =>
      ResponseFaceRecognitionModel(
        message: json["message"],
        filename: json["filename"],
        accuration: json["accuration"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "filename": filename,
        "accuration": accuration,
      };
}
