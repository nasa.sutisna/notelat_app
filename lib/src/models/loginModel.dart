// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  String message;
  Data data;

  LoginModel({
    this.message,
    this.data,
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  String accessToken;
  int userId;
  String email;
  int role;
  Employee employee;

  Data({
    this.accessToken,
    this.userId,
    this.email,
    this.role,
    this.employee,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accessToken: json["accessToken"],
        userId: json["userId"],
        email: json["email"],
        role: json["role"],
        employee: Employee.fromJson(json["employee"]),
      );

  Map<String, dynamic> toJson() => {
        "accessToken": accessToken,
        "userId": userId,
        "email": email,
        "role": role,
        "employee": employee.toJson(),
      };
}

class Employee {
  String employeeId;
  String fullName;
  String employeeNumber;
  String email;
  String phoneNumber;
  String position;
  String photo;
  DateTime createdAt;
  DateTime updatedAt;

  Employee({
    this.employeeId,
    this.fullName,
    this.employeeNumber,
    this.email,
    this.phoneNumber,
    this.position,
    this.photo,
    this.createdAt,
    this.updatedAt,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        employeeId: json["employeeId"],
        fullName: json["fullName"],
        employeeNumber: json["employeeNumber"],
        email: json["email"],
        phoneNumber: json["phoneNumber"],
        position: json["position"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "employeeId": employeeId,
        "fullName": fullName,
        "employeeNumber": employeeNumber,
        "email": email,
        "phoneNumber": phoneNumber,
        "position": position,
        "photo": photo,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
      };
}
