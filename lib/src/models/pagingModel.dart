// To parse this JSON data, do
//
//     final pagingModel = pagingModelFromJson(jsonString);

import 'dart:convert';

PagingModel pagingModelFromJson(String str) =>
    PagingModel.fromJson(json.decode(str));

String pagingModelToJson(PagingModel data) => json.encode(data.toJson());

class PagingModel<T> {
  int page;
  int limit;
  int total;
  int totalPage;
  List<T> data;

  PagingModel({
    this.page,
    this.limit,
    this.total,
    this.totalPage,
    this.data,
  });

  factory PagingModel.fromJson(Map<String, dynamic> json) => PagingModel(
        page: json["page"],
        limit: json["limit"],
        total: json["total"],
        totalPage: json["totalPage"],
        data: List<T>.from(json["data"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "limit": limit,
        "total": total,
        "totalPage": totalPage,
        "data": List<T>.from(data.map((x) => x)),
      };
}
