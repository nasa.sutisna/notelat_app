// To parse this JSON data, do
//
//     final shiftModel = shiftModelFromJson(jsonString);

import 'dart:convert';

ShiftModel shiftModelFromJson(String str) => ShiftModel.fromJson(json.decode(str));

String shiftModelToJson(ShiftModel data) => json.encode(data.toJson());

class ShiftModel {
    int page;
    int limit;
    int total;
    int totalPage;
    List<DataShift> data;

    ShiftModel({
        this.page,
        this.limit,
        this.total,
        this.totalPage,
        this.data,
    });

    factory ShiftModel.fromJson(Map<String, dynamic> json) => ShiftModel(
        page: json["page"],
        limit: json["limit"],
        total: json["total"],
        totalPage: json["totalPage"],
        data: List<DataShift>.from(json["data"].map((x) => DataShift.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "page": page,
        "limit": limit,
        "total": total,
        "totalPage": totalPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class DataShift {
    String shiftCode;
    String shiftName;
    DateTime shiftStartTime;
    DateTime shiftEndTime;
    DateTime createdAt;
    DateTime updatedAt;

    DataShift({
        this.shiftCode,
        this.shiftName,
        this.shiftStartTime,
        this.shiftEndTime,
        this.createdAt,
        this.updatedAt,
    });

    factory DataShift.fromJson(Map<String, dynamic> json) => DataShift(
        shiftCode: json["shiftCode"],
        shiftName: json["shiftName"],
        shiftStartTime: DateTime.parse(json["shiftStartTime"]),
        shiftEndTime: DateTime.parse(json["shiftEndTime"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "shiftCode": shiftCode,
        "shiftName": shiftName,
        "shiftStartTime": shiftStartTime.toIso8601String(),
        "shiftEndTime": shiftEndTime.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
    };
}
