// To parse this JSON data, do
//
//     final responseSucessModel = responseSucessModelFromJson(jsonString);

import 'dart:convert';

ResponseSucessModel responseSucessModelFromJson(String str) => ResponseSucessModel.fromJson(json.decode(str));

String responseSucessModelToJson(ResponseSucessModel data) => json.encode(data.toJson());

class ResponseSucessModel {
    String message;
    dynamic data;

    ResponseSucessModel({
        this.message,
        this.data,
    });

    factory ResponseSucessModel.fromJson(Map<String, dynamic> json) => ResponseSucessModel(
        message: json["message"],
        data: json["data"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data,
    };
}
