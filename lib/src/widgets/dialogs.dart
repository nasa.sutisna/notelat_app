import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dialogs {
  confirmResult(bool result, BuildContext context) {
    if (result == true) {
      Navigator.pop(context , true);
    } else {
      Navigator.pop(context, false);
    }
  }

  confirm(BuildContext context, String title, String desc) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[Text(desc)],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => confirmResult(false, context),
                  child: Text('Batal')),
              FlatButton(
                  onPressed: () => confirmResult(true, context),
                  child: Text('Ya')),
            ],
          );
        });
  }

  warning(BuildContext context, String title, String desc) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[Text(desc)],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => confirmResult(true, context),
                  child: Text('Oke')),
            ],
          );
        });
  }
}
