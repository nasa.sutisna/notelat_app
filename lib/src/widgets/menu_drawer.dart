import 'package:flutter/material.dart';
import 'package:notelat_app/src/blocs/homeBloc.dart';
import 'package:notelat_app/src/blocs/loginBloc.dart';
import 'package:notelat_app/src/screens/changePassword.dart';
import 'package:notelat_app/src/screens/home.dart';
import 'package:notelat_app/src/screens/login.dart';
import 'package:notelat_app/src/screens/screen_admin/employee/employee.dart';
import 'package:notelat_app/src/screens/screen_admin/shift/shift.dart';
import 'package:notelat_app/src/screens/screen_admin/shift_group/shiftGroup.dart';
import 'package:notelat_app/src/screens/screen_admin/worklocation/worklocation.dart';
import 'package:notelat_app/src/screens/screen_admin/report/reportAttendance.dart';
import 'package:notelat_app/src/utills/storagePreferences.dart';

class MenuDrawer extends StatefulWidget {
  @override
  _MenuDrawerState createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  dynamic employee;
  String photo;
  int role;
  bool isLoading = false;

  @override
  void initState() {
    getUserData();
    super.initState();
  }

  @override
  void dispose() {
    homeBloc.dispose();
    super.dispose();
  }


  Future _logout() async {
    try {
      setState(() {
        isLoading = true;
      });

      var token = await StoragePreferences.getToken();
      await loginBloc.doLogout(token);
      await StoragePreferences.clearStorage();
      setState(() {
        isLoading = false;
      });

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Login()));
    } catch (error) {
      await StoragePreferences.clearStorage();
      setState(() {
        isLoading = false;
      });

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Login()));
    }
  }


  getUserData() async {
    var emp = await homeBloc.getUserData();
    var getRole = await homeBloc.getRole();

    setState(() {
      employee = emp;
      role = getRole;
    });
    await getPhotoProfile();
  }

  getPhotoProfile() async {
    var getPhoto = await homeBloc.showImageProfile(employee["photo"]);
    setState(() {
      photo = getPhoto;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(child: role == 1 ? menuAdmin() : menuUser());
  }

  Widget menuAdmin() {
    return ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text(
            employee != null ? employee['fullName'] : '',
          ),
          accountEmail: Text(
            employee != null ? employee['email'] : '',
          ),
          currentAccountPicture: CircleAvatar(
            backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                ? Colors.blue
                : Colors.white,
            child: employee == null || employee["photo"] == null
                ? ClipOval(
                    child: Image.asset("assets/default.png",
                        height: 130.0, width: 135.0, fit: BoxFit.fill))
                : ClipOval(
                    child: FadeInImage.assetNetwork(
                    placeholder: 'assets/default.png',
                    height: 130.0,
                    width: 135.0,
                    image: photo,
                    fit: BoxFit.fill,
                  )),
          ),
        ),
        ListTile(
          leading: Icon(Icons.contacts),
          title: Text("Karyawan"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                settings: RouteSettings(name: "employeeList"),
                builder: (context) => EmployeePage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.access_alarm),
          title: Text("Shift"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => ShiftPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.group_work),
          title: Text("Shift Group"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => ShiftGroupPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.map),
          title: Text("Lokasi Kerja"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => WorklocationPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.print),
          title: Text("Cetak Laporan"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ReportAttendancePage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text("Keluar"),
          onTap: () async {
            await _logout();
          },
        )
      ],
    );
  }

  Widget menuUser() {
    return ListView(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 10.0)),
        ListTile(
          leading: Icon(Icons.lock),
          title: Text("Ubah Password"),
          onTap: () {
            Navigator.of(context).pop();
            print(context);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ChangePasswordPage()));
          },
        ),
      ],
    );
  }
}
